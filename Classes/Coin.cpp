#include "Coin.h"
#include "Definitions.h"

USING_NS_CC;
unsigned int spawntag2;

Coin::Coin( )
{
    visibleSize = Director::getInstance( )->getVisibleSize( );
    origin = Director::getInstance( )->getVisibleOrigin( );
}

void Coin::SpawnPipe2( cocos2d::Layer *layer, unsigned int coinSpawnNum)
{
    visibleSize = Director::getInstance( )->getVisibleSize( );
    origin = Director::getInstance( )->getVisibleOrigin( );
    
    //Set Resolution
    screenWidth = visibleSize.width;
    screenHeight = visibleSize.height;
    
    auto lane = Sprite::create("bg2.png");
    laneWidth=lane->getContentSize().width;
    laneHeight=lane->getContentSize().height;
    
    spawntag2 = coinSpawnNum;
    
    auto enemyEgg = Sprite::create( "egg.png" );
    coin = Sprite::create("coin.png");
    auto coinBody = PhysicsBody::createCircle( coin->getContentSize( ).width / 2, PhysicsMaterial(0,0,0) );
    //coinBody->setDynamic( false );
    coinBody->setCollisionBitmask( COIN_COLLISION_BITMASK );
    coinBody->setContactTestBitmask( true );
    
    coin->setPhysicsBody(coinBody);
    
    float pipe_speed;
    if(visibleSize.height > LARGE_HEIGHT)
    {
        pipe_speed = 0.0037;
    }else{
        pipe_speed = 0.0075;
    }
    
    SpriteFrameCache* cache = SpriteFrameCache::getInstance();
    cache->addSpriteFramesWithFile("eggrush_run.plist");
    Vector<SpriteFrame*> animFrames3(6);
    char str3[100] = {0};
    for(int i = 1; i <=6; i++)
    {
        sprintf(str3, "coin-%d",i);
        SpriteFrame* frame3 = cache->getSpriteFrameByName( str3 );
        animFrames3.pushBack(frame3);
    }
    auto animation3 = Animation::createWithSpriteFrames(animFrames3, 0.05f);
    coin->runAction(RepeatForever::create(Animate::create(animation3)));
    coinBody->setTag(spawntag2);
    coin->setTag(spawntag2);
    coin->setName("coin");
    
    
    auto random = CCRANDOM_0_1( );
    
    if ( random < 0.20 )
    {
        Position = LanePosition::ONE;
    }
    else if ( random >=0.20 && random < 0.40)
    {
        Position = LanePosition::TWO;
    }
    else if ( random >=0.40 && random < 0.60)
    {
        Position = LanePosition::THREE;
    }
    else if ( random >=0.60 && random < 0.80)
    {
        Position = LanePosition::FOUR;
    }
    else if ( random >=0.80)
    {
        Position = LanePosition::FIVE;
    }
    
    
    if (Position == LanePosition::ONE)
    {
        coin->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
        layer->addChild( coin, COIN_LAYER);
        auto coinAction = MoveTo::create( 4  , Point(screenWidth/2 - 0.4 * laneWidth , -visibleSize.height/2 ) );
        coin->runAction( coinAction );
    }
    if (Position == LanePosition::TWO)
    {
        coin->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2) );
        layer->addChild( coin, COIN_LAYER);
        auto coinAction = MoveTo::create( 4 , Point( screenWidth/2 - 0.2 * laneWidth , -visibleSize.height/2 ) );
        coin->runAction( coinAction );
    }
    if (Position == LanePosition::THREE)
    {
        coin->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
        layer->addChild( coin, COIN_LAYER);
        auto coinAction = MoveTo::create( 4 , Point( screenWidth/2, -visibleSize.height/2 ) );
        coin->runAction( coinAction );
    }
    if (Position == LanePosition::FOUR)
    {
        coin->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2) );
        layer->addChild( coin, COIN_LAYER);
        auto coinAction = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
        coin->runAction( coinAction );
    }
    if (Position == LanePosition::FIVE)
    {
        coin->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
        layer->addChild( coin, COIN_LAYER);
        auto coinAction = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
        coin->runAction( coinAction );
    }

}
void Coin::SpawnMagnet( cocos2d::Layer *layer, unsigned int coinSpawnNum)
{
    visibleSize = Director::getInstance( )->getVisibleSize( );
    origin = Director::getInstance( )->getVisibleOrigin( );
    
    //Set Resolution
    screenWidth = visibleSize.width;
    screenHeight = visibleSize.height;
    
    auto lane = Sprite::create("bg2.png");
    laneWidth=lane->getContentSize().width;
    laneHeight=lane->getContentSize().height;
    
    spawntag2 = coinSpawnNum;
    
    auto enemyEgg = Sprite::create( "egg.png" );
    coin = Sprite::create("magnet.png");
    auto coinBody = PhysicsBody::createCircle( coin->getContentSize( ).width / 2, PhysicsMaterial(0,0,0) );
    //coinBody->setDynamic( false );
    coinBody->setCollisionBitmask( MAGNET_COLLISION_BITMASK );
    coinBody->setContactTestBitmask( true );
    
    coin->setPhysicsBody(coinBody);
    
    float pipe_speed;
    if(visibleSize.height > LARGE_HEIGHT)
    {
        pipe_speed = 0.0037;
    }else{
        pipe_speed = 0.0075;
    }
    

    coinBody->setTag(spawntag2);
    coin->setTag(spawntag2);
    coin->setName("coin");
    
    auto random = CCRANDOM_0_1( );
    
    if ( random < 0.20 )
    {
        Position = LanePosition::ONE;
    }
    else if ( random >=0.20 && random < 0.40)
    {
        Position = LanePosition::TWO;
    }
    else if ( random >=0.40 && random < 0.60)
    {
        Position = LanePosition::THREE;
    }
    else if ( random >=0.60 && random < 0.80)
    {
        Position = LanePosition::FOUR;
    }
    else if ( random >=0.80)
    {
        Position = LanePosition::FIVE;
    }
    
    
    if (Position == LanePosition::ONE)
    {
        coin->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
        layer->addChild( coin, COIN_LAYER);
        auto coinAction = MoveTo::create( 4  , Point(screenWidth/2 - 0.4 * laneWidth , -visibleSize.height/2 ) );
        coin->runAction( coinAction );
    }
    if (Position == LanePosition::TWO)
    {
        coin->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
        layer->addChild( coin, COIN_LAYER);
        auto coinAction = MoveTo::create( 4 , Point( screenWidth/2 - 0.2 * laneWidth , -visibleSize.height/2 ) );
        coin->runAction( coinAction );
    }
    if (Position == LanePosition::THREE)
    {
        coin->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
        layer->addChild( coin, COIN_LAYER);
        auto coinAction = MoveTo::create( 4 , Point( screenWidth/2, -visibleSize.height/2 ) );
        coin->runAction( coinAction );
    }
    if (Position == LanePosition::FOUR)
    {
        coin->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
        layer->addChild( coin, COIN_LAYER);
        auto coinAction = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
        coin->runAction( coinAction );
    }
    if (Position == LanePosition::FIVE)
    {
        coin->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
        layer->addChild( coin, COIN_LAYER);
        auto coinAction = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
        coin->runAction( coinAction );
    }
    
}
void Coin::SpawnInvi( cocos2d::Layer *layer, unsigned int coinSpawnNum)
{
    visibleSize = Director::getInstance( )->getVisibleSize( );
    origin = Director::getInstance( )->getVisibleOrigin( );
    
    //Set Resolution
    screenWidth = visibleSize.width;
    screenHeight = visibleSize.height;
    
    auto lane = Sprite::create("bg2.png");
    laneWidth=lane->getContentSize().width;
    laneHeight=lane->getContentSize().height;
    
    spawntag2 = coinSpawnNum;
    
    auto enemyEgg = Sprite::create( "egg.png" );
    coin = Sprite::create("invi.png");
    auto coinBody = PhysicsBody::createCircle( coin->getContentSize( ).width / 2, PhysicsMaterial(0,0,0) );
    //coinBody->setDynamic( false );
    coinBody->setCollisionBitmask( INVI_COLLISION_BITMASK );
    coinBody->setContactTestBitmask( true );
    
    coin->setPhysicsBody(coinBody);
    
    float pipe_speed;
    if(visibleSize.height > LARGE_HEIGHT)
    {
        pipe_speed = 0.0037;
    }else{
        pipe_speed = 0.0075;
    }
    
    
    coinBody->setTag(spawntag2);
    coin->setTag(spawntag2);
    coin->setName("coin");
    
    auto random = CCRANDOM_0_1( );
    
    if ( random < 0.20 )
    {
        Position = LanePosition::ONE;
    }
    else if ( random >=0.20 && random < 0.40)
    {
        Position = LanePosition::TWO;
    }
    else if ( random >=0.40 && random < 0.60)
    {
        Position = LanePosition::THREE;
    }
    else if ( random >=0.60 && random < 0.80)
    {
        Position = LanePosition::FOUR;
    }
    else if ( random >=0.80)
    {
        Position = LanePosition::FIVE;
    }
    
    
    if (Position == LanePosition::ONE)
    {
        coin->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
        layer->addChild( coin, COIN_LAYER);
        auto coinAction = MoveTo::create( 4  , Point(screenWidth/2 - 0.4 * laneWidth , -visibleSize.height/2 ) );
        coin->runAction( coinAction );
    }
    if (Position == LanePosition::TWO)
    {
        coin->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
        layer->addChild( coin, COIN_LAYER);
        auto coinAction = MoveTo::create( 4 , Point( screenWidth/2 - 0.2 * laneWidth , -visibleSize.height/2 ) );
        coin->runAction( coinAction );
    }
    if (Position == LanePosition::THREE)
    {
        coin->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
        layer->addChild( coin, COIN_LAYER);
        auto coinAction = MoveTo::create( 4 , Point( screenWidth/2, -visibleSize.height/2 ) );
        coin->runAction( coinAction );
    }
    if (Position == LanePosition::FOUR)
    {
        coin->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
        layer->addChild( coin, COIN_LAYER);
        auto coinAction = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
        coin->runAction( coinAction );
    }
    if (Position == LanePosition::FIVE)
    {
        coin->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
        layer->addChild( coin, COIN_LAYER);
        auto coinAction = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
        coin->runAction( coinAction );
    }
    
}
void Coin::SpawnLife( cocos2d::Layer *layer, unsigned int coinSpawnNum)
{
    visibleSize = Director::getInstance( )->getVisibleSize( );
    origin = Director::getInstance( )->getVisibleOrigin( );
    
    //Set Resolution
    screenWidth = visibleSize.width;
    screenHeight = visibleSize.height;
    
    auto lane = Sprite::create("bg2.png");
    laneWidth=lane->getContentSize().width;
    laneHeight=lane->getContentSize().height;
    
    spawntag2 = coinSpawnNum;
    
    auto enemyEgg = Sprite::create( "egg.png" );
    coin = Sprite::create("life.png");
    auto coinBody = PhysicsBody::createCircle( coin->getContentSize( ).width / 2, PhysicsMaterial(0,0,0) );
    //coinBody->setDynamic( false );
    coinBody->setCollisionBitmask( LIFE_COLLISION_BITMASK );
    coinBody->setContactTestBitmask( true );
    
    coin->setPhysicsBody(coinBody);
    
    float pipe_speed;
    if(visibleSize.height > LARGE_HEIGHT)
    {
        pipe_speed = 0.0037;
    }else{
        pipe_speed = 0.0075;
    }
    
    
    coinBody->setTag(spawntag2);
    coin->setTag(spawntag2);
    coin->setName("coin");
    
    auto random = CCRANDOM_0_1( );
    
    if ( random < 0.20 )
    {
        Position = LanePosition::ONE;
    }
    else if ( random >=0.20 && random < 0.40)
    {
        Position = LanePosition::TWO;
    }
    else if ( random >=0.40 && random < 0.60)
    {
        Position = LanePosition::THREE;
    }
    else if ( random >=0.60 && random < 0.80)
    {
        Position = LanePosition::FOUR;
    }
    else if ( random >=0.80)
    {
        Position = LanePosition::FIVE;
    }
    
    
    if (Position == LanePosition::ONE)
    {
        coin->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
        layer->addChild( coin, COIN_LAYER);
        auto coinAction = MoveTo::create( 4  , Point(screenWidth/2 - 0.4 * laneWidth , -visibleSize.height/2 ) );
        coin->runAction( coinAction );
    }
    if (Position == LanePosition::TWO)
    {
        coin->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
        layer->addChild( coin, COIN_LAYER);
        auto coinAction = MoveTo::create( 4 , Point( screenWidth/2 - 0.2 * laneWidth , -visibleSize.height/2 ) );
        coin->runAction( coinAction );
    }
    if (Position == LanePosition::THREE)
    {
        coin->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
        layer->addChild( coin, COIN_LAYER);
        auto coinAction = MoveTo::create( 4 , Point( screenWidth/2, -visibleSize.height/2 ) );
        coin->runAction( coinAction );
    }
    if (Position == LanePosition::FOUR)
    {
        coin->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
        layer->addChild( coin, COIN_LAYER);
        auto coinAction = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
        coin->runAction( coinAction );
    }
    if (Position == LanePosition::FIVE)
    {
        coin->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
        layer->addChild( coin, COIN_LAYER);
        auto coinAction = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
        coin->runAction( coinAction );
    }
    
}

