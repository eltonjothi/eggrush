#ifndef __ENEMY_H__
#define __ENEMY_H__

#include "cocos2d.h"
#include "MyBodyParser.h"

class Enemy
{
public:
    Enemy();
    float speedInc;
    void SpawnEnemy( cocos2d::Layer *layer, unsigned int spawnNum);
    void SpawnObstacle( cocos2d::Layer *layer, unsigned int spawnNum);
    void SpawnCutter( cocos2d::Layer *layer, unsigned int spawnNum);
    void SpawnBlender( cocos2d::Layer *layer, unsigned int spawnNum);
    void SpawnFlamer( cocos2d::Layer *layer, unsigned int spawnNum);
	cocos2d::Sprite *egg;
    cocos2d::Sprite *enemyObstacle1;
    cocos2d::Sprite *enemyObstacle1Support;
    cocos2d::Sprite *enemyObstacle1Shade;
	cocos2d::PhysicsBody *eggBody;
	cocos2d::Sprite *enemyEgg;
    cocos2d::Sprite *enemyEgg2;
    cocos2d::Sprite *enemyEgg3;
	cocos2d::Node *pointNode;
    
    cocos2d::Animate *animate;
    
    cocos2d::Sprite *coin;
    cocos2d::PhysicsBody *coinBody;
    
    float screenWidth;
    float screenHeight;
    
    float laneWidth;
    float laneHeight;
    enum class LanePosition {
        ONE,
        TWO,
        THREE,
        FOUR,
        FIVE
    };
    
    LanePosition Position;
    
private:
    cocos2d::Size visibleSize;
    cocos2d::Vec2 origin;
    
};

#endif // __PIPE_H__
