#ifndef __COIN_H__
#define __COIN_H__

#include "cocos2d.h"
#include "MyBodyParser.h"

class Coin
{
public:
    Coin();

    void SpawnPipe2( cocos2d::Layer *layer, unsigned int coinSpawnNum);
    void SpawnMagnet( cocos2d::Layer *layer, unsigned int coinSpawnNum);
    void SpawnInvi( cocos2d::Layer *layer, unsigned int coinSpawnNum);
    void SpawnLife( cocos2d::Layer *layer, unsigned int coinSpawnNum);

    cocos2d::Sprite *coin;
    cocos2d::PhysicsBody *coinBody;
    
    float screenWidth;
    float screenHeight;
    
    float laneWidth;
    float laneHeight;
    enum class LanePosition {
        ONE,
        TWO,
        THREE,
        FOUR,
        FIVE
    };
    
    LanePosition Position;
private:
    cocos2d::Size visibleSize;
    cocos2d::Vec2 origin;
    
};

#endif // __PIPE_H__
