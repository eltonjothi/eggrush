#include "Hero.h"
#include "Definitions.h"

USING_NS_CC;

Hero::Hero( cocos2d::Layer *layer )
{
    visibleSize = Director::getInstance( )->getVisibleSize( );
    origin = Director::getInstance( )->getVisibleOrigin( );
    
    Position = LanePosition::THREE;

    //Set Resolution
    screenWidth = visibleSize.width;
    screenHeight = visibleSize.height;
    
    isInvi = false;
    isJump = false;
    isMagnet = false;
    
    hero = Sprite::create( "egg.png" );
    hero->setPosition(Point(screenWidth/2, screenHeight/4));
    
    lane = Sprite::create("bg2.png");
    laneWidth=lane->getContentSize().width;
    laneHeight=lane->getContentSize().height;
    

	heroBody = PhysicsBody::createCircle( hero->getContentSize( ).width / 4, PhysicsMaterial(0,0,0) );
    heroBody->setCollisionBitmask( BIRD_COLLISION_BITMASK );
    heroBody->setContactTestBitmask( true );
    //heroBody->applyImpulse(Vect(0,-250));
    hero->setPhysicsBody( heroBody );
	hero->setName("hero");
    hero->setScale(0.8);
    layer->addChild( hero, HERO_LAYER);
    
    
    SpriteFrameCache* cache = SpriteFrameCache::getInstance();
    cache->addSpriteFramesWithFile("eggrush_run.plist");
    
    Vector<SpriteFrame*> animFrames(3);
    char str[100] = {0};
    for(int i = 1; i <=3; i++)
    {
        sprintf(str, "egg1-%d",i);
        SpriteFrame* frame = cache->getSpriteFrameByName( str );
        animFrames.pushBack(frame);
    }
    auto animation = Animation::createWithSpriteFrames(animFrames, 0.05f);
    animate = Animate::create(animation);
    hero->runAction(RepeatForever::create(Animate::create(animation)));
    //auto action = TintTo::create(3, 255, 0, 0);
    //hero->runAction(action);
    //isFalling = true;
}

//SIMPLE JUMP CODE
//JumpTo* jumpTo = JumpTo::create(0.5,Point(screenWidth/2 + 0.2 * laneWidth, screenHeight - screenHeight/4),screenHeight/10,1);
void Hero::JumpLeft(){
auto jumpCallback = CallFunc::create( [this]() {
    this->JumpCallback();
});
    if(Position == LanePosition::FIVE){
        ActionInterval*  rotate45 = RotateTo::create( 0.25, 45);
        ActionInterval*  rotate0 = RotateTo::create(0.25 , 0);
        ActionInterval*  lerp = Sequence::create(rotate45, rotate0, jumpCallback, NULL);
        Action*  action = Spawn::create(JumpTo::create(0.5,Point(screenWidth/2 + 0.2 * laneWidth, screenHeight/4),screenHeight/10,1),lerp,NULL);
        if(isJump==false)
        {
            isJump=true;
            hero->runAction(action);
            Position = LanePosition::FOUR;
        }
    }else if(Position == LanePosition::FOUR){
        ActionInterval*  rotate45 = RotateTo::create( 0.25, 45);
        ActionInterval*  rotate0 = RotateTo::create(0.25 , 0);
        ActionInterval*  lerp = Sequence::create(rotate45, rotate0, jumpCallback, NULL);
        Action*  action = Spawn::create(JumpTo::create(0.5,Point(screenWidth/2, screenHeight/4),screenHeight/10,1),lerp,NULL);
        if(isJump==false)
        {
            isJump=true;
            hero->runAction(action);
            Position = LanePosition::THREE;
        }
    }else if(Position == LanePosition::THREE){
        ActionInterval*  rotate45 = RotateTo::create( 0.25, 45);
        ActionInterval*  rotate0 = RotateTo::create(0.25 , 0);
        ActionInterval*  lerp = Sequence::create(rotate45, rotate0, jumpCallback, NULL);
        //Action*  action = Spawn::create(JumpTo::create(0.5,Point(screenWidth/2 - 0.2 * laneWidth, screenHeight - screenHeight/4),screenHeight/10,1),lerp,NULL);
        Action*  action = Spawn::create(JumpTo::create(0.5,Point(screenWidth/2 - 0.2 * laneWidth, screenHeight/4),screenHeight/10,1),lerp,NULL);
        if(isJump==false)
        {
            isJump=true;
            hero->runAction(action);
            Position = LanePosition::TWO;
        }
    }else if(Position == LanePosition::TWO){
        ActionInterval*  rotate45 = RotateTo::create( 0.25, 45);
        ActionInterval*  rotate0 = RotateTo::create(0.25 , 0);
        ActionInterval*  lerp = Sequence::create(rotate45, rotate0, jumpCallback, NULL);
        Action*  action = Spawn::create(JumpTo::create(0.5,Point(screenWidth/2 - 0.4 * laneWidth, screenHeight/4),screenHeight/10,1),lerp,NULL);
        if(isJump==false)
        {
            isJump=true;
            hero->runAction(action);
            Position = LanePosition::ONE;
        }
    }
}

void Hero::JumpRight(){
auto jumpCallback = CallFunc::create( [this]() {
    this->JumpCallback();
});
    if(Position == LanePosition::ONE){
        ActionInterval*  rotate45 = RotateTo::create( 0.25, -45);
        ActionInterval*  rotate0 = RotateTo::create(0.25 , 0);
        ActionInterval*  lerp = Sequence::create(rotate45, rotate0, jumpCallback, NULL);
        Action*  action = Spawn::create(JumpTo::create(0.5,Point(screenWidth/2 - 0.2 * laneWidth, screenHeight/4),screenHeight/10,1),lerp,NULL);
        if(isJump==false)
        {
            isJump=true;
            hero->runAction(action);
            Position = LanePosition::TWO;
        }
    }else if(Position == LanePosition::TWO){
        ActionInterval*  rotate45 = RotateTo::create( 0.25, -45);
        ActionInterval*  rotate0 = RotateTo::create(0.25 , 0);
        ActionInterval*  lerp = Sequence::create(rotate45, rotate0, jumpCallback, NULL);
        Action*  action = Spawn::create(JumpTo::create(0.5,Point(screenWidth/2, screenHeight/4),screenHeight/10,1),lerp,NULL);
        if(isJump==false)
        {
            isJump=true;
            hero->runAction(action);
            Position = LanePosition::THREE;
        }
    }else if(Position == LanePosition::THREE){
        ActionInterval*  rotate45 = RotateTo::create( 0.25, -45);
        ActionInterval*  rotate0 = RotateTo::create(0.25 , 0);
        ActionInterval*  lerp = Sequence::create(rotate45, rotate0, jumpCallback, NULL);
        Action*  action = Spawn::create(JumpTo::create(0.5,Point(screenWidth/2 + 0.2 * laneWidth, screenHeight/4),screenHeight/10,1),lerp,NULL);
        if(isJump==false)
        {
            isJump=true;
            hero->runAction(action);
            Position = LanePosition::FOUR;
        }
    }else if(Position == LanePosition::FOUR){
        ActionInterval*  rotate45 = RotateTo::create( 0.25, -45);
        ActionInterval*  rotate0 = RotateTo::create(0.25 , 0);
        ActionInterval*  lerp = Sequence::create(rotate45, rotate0, jumpCallback, NULL);
        Action*  action = Spawn::create(JumpTo::create(0.5,Point(screenWidth/2 + 0.4 * laneWidth, screenHeight/4),screenHeight/10,1),lerp,NULL);
        if(isJump==false)
        {
            isJump=true;
            hero->runAction(action);
            Position = LanePosition::FIVE;
        }
    }
}

void Hero::RotateJumpLeft(){
auto jumpCallback = CallFunc::create( [this]() {
    this->JumpCallback();
});
    if(Position == LanePosition::FIVE){
        ActionInterval*  sequence = Sequence::create(JumpTo::create(0.5,Point(screenWidth/2 + 0.2 * laneWidth, screenHeight/4),screenHeight/10,1), jumpCallback, NULL);
        Action*  action = Spawn::create(sequence, RotateBy::create(0.5,  -360),NULL);
        if(isJump==false)
        {
            isJump=true;
            hero->runAction(action);
            Position = LanePosition::FOUR;
        }
    }else if(Position == LanePosition::FOUR){
        ActionInterval*  sequence = Sequence::create(JumpTo::create(0.5,Point(screenWidth/2, screenHeight/4),screenHeight/10,1), jumpCallback, NULL);
        Action*  action = Spawn::create(sequence, RotateBy::create(0.5,  -360),NULL);
        if(isJump==false)
        {
            isJump=true;
            hero->runAction(action);
            Position = LanePosition::THREE;
        }
    }else if(Position == LanePosition::THREE){
        ActionInterval*  sequence = Sequence::create(JumpTo::create(0.5,Point(screenWidth/2 - 0.2 * laneWidth, screenHeight/4),screenHeight/10,1), jumpCallback, NULL);
        Action*  action = Spawn::create(sequence, RotateBy::create(0.5,  -360),NULL);
        if(isJump==false)
        {
            isJump=true;
            hero->runAction(action);
            Position = LanePosition::TWO;
        }
    }else if(Position == LanePosition::TWO){
        
        ActionInterval*  sequence = Sequence::create(JumpTo::create(0.5,Point(screenWidth/2 - 0.4 * laneWidth, screenHeight/4),screenHeight/10,1), jumpCallback, NULL);
        Action*  action = Spawn::create(sequence, RotateBy::create(0.5,  -360),NULL);
        if(isJump==false)
        {
            isJump=true;
            hero->runAction(action);
            Position = LanePosition::ONE;
        }
    }

}
void Hero::RotateJumpRight(){
auto jumpCallback = CallFunc::create( [this]() {
    this->JumpCallback();
});
    if(Position == LanePosition::ONE){
        
        ActionInterval*  sequence = Sequence::create(JumpTo::create(0.5,Point(screenWidth/2 - 0.2 * laneWidth, screenHeight/4),screenHeight/10,1), jumpCallback, NULL);
        Action*  action = Spawn::create(sequence, RotateBy::create(0.5,  360),NULL);
        if(isJump==false)
        {
            isJump=true;
            hero->runAction(action);
            Position = LanePosition::TWO;
        }
    }else if(Position == LanePosition::TWO){
        ActionInterval*  sequence = Sequence::create(JumpTo::create(0.5,Point(screenWidth/2, screenHeight/4),screenHeight/10,1), jumpCallback, NULL);
        Action*  action = Spawn::create(sequence, RotateBy::create(0.5,  360),NULL);
        if(isJump==false)
        {
            isJump=true;
            hero->runAction(action);
            Position = LanePosition::THREE;
        }
    }else if(Position == LanePosition::THREE){
        ActionInterval*  sequence = Sequence::create(JumpTo::create(0.5,Point(screenWidth/2 + 0.2 * laneWidth, screenHeight/4),screenHeight/10,1), jumpCallback, NULL);
        Action*  action = Spawn::create(sequence, RotateBy::create(0.5,  360),NULL);
        if(isJump==false)
        {
            isJump=true;
            hero->runAction(action);
            Position = LanePosition::FOUR;
        }
    }else if(Position == LanePosition::FOUR){
        ActionInterval*  sequence = Sequence::create(JumpTo::create(0.5,Point(screenWidth/2 + 0.4 * laneWidth, screenHeight/4),screenHeight/10,1), jumpCallback, NULL);
        Action*  action = Spawn::create(sequence, RotateBy::create(0.5,  360),NULL);
        if(isJump==false)
        {
            isJump=true;
            hero->runAction(action);
            Position = LanePosition::FIVE;
        }
    }
}

void Hero::Invisible(){
    isInvi = true;
    //hero->getPhysicsBody()->setEnable(false);
    
    auto blink = Blink::create(9, 27);  //10 Blinks in 5 secs
    auto blink2 = Blink::create(1, 10);
    //auto blink3 = Blink::create(2, 10);
    auto show = Show::create();
    auto callback = CallFunc::create( [this]() {
        this->InvisibleCallback();
    });
    
    auto Tint = TintBy::create(10, 255, 255, 0);
    auto spawn = Spawn::create(blink,Tint, NULL);
    auto sequence = Sequence::create(spawn,blink2,show, callback, NULL);
    hero->runAction(sequence);
    

    //this->getChildByName("hero")->getPhysicsBody()->setEnable(false);
    //layer->getName("coin")->runAction(Follow::create(hero->hero));
    //layer->getChildByName("coin")->runAction(Follow::create(hero));
    //Action* actionMove = MoveTo::create(2,Point(0, 0) );
}

void Hero::InvisibleCallback( )
{
    isInvi = false;
    //hero->getPhysicsBody()->setEnable(true);
};

void Hero::CollideInvisible(){
    isInvi = true;
    hero->getPhysicsBody()->setEnable(false);
    auto blink2 = Blink::create(2, 6);
    auto blink3 = Blink::create(1, 5);
    auto show = Show::create();
    auto callback = CallFunc::create( [this]() {
        this->CollideInvisibleCallback();
    });
    
    auto sequence = Sequence::create(blink2,blink3,show, callback, NULL);
    hero->runAction(sequence);
}

void Hero::CollideInvisibleCallback( )
{
    isInvi = false;
    hero->getPhysicsBody()->setEnable(true);
}

void Hero::JumpCallback()
{
    isJump = false;
}

void Hero::Magnet()
{
    isMagnet = true;
    auto Tint = TintBy::create(10, 0, 255, 255);
    auto TintReverse = Tint->reverse();
    
    //auto  blink = Blink::create(6, 20);  //10 Blinks in 5 secs
    auto callback = CallFunc::create( [this]() {
        this->MagnetCallback();
    });
    auto sequence = Sequence::create(Tint, callback, NULL);
    hero->runAction(sequence);
}
void Hero::MagnetCallback()
{
    isMagnet = false;
}


