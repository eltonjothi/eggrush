#include "SplashScreenScene.h"
#include "MainMenuScene.h"
#include "Definitions.h"

USING_NS_CC;

Scene* SplashScreen::createScene()
{
    auto scene = Scene::create();
    auto layer = SplashScreen::create();
	scene->addChild(layer);
    return scene;
}

// on "init" you need to initialize your instance
bool SplashScreen::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	//this->scheduleOnce( schedule_selector( SplashScreen::GoToMainMenuScene ), DISPLAY_TIME_SPLASH_SCENE );

    auto backgroundSprite = Sprite::create( "loader4.png" );
    backgroundSprite->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y) );
    this->addChild( backgroundSprite, 0);
    
    auto barSprite = Sprite::create( "loader3.png" );
    barSprite->setPosition(Point(visibleSize.width / 2.0f, visibleSize.height / 2.0f + barSprite->getContentSize().height / 4.0f));
    this->addChild( barSprite, 1);
    auto barSprite2 = Sprite::create( "loader1.png" );
    barSprite2->setPosition(Point(visibleSize.width / 2.0f, visibleSize.height / 2.0f + barSprite2->getContentSize().height / 4.0f));
    this->addChild( barSprite2, 2);
    //uiLayer = Layer::create();
    loadingBar = LoadingBar::create();
    loadingBar->setName("LoadingBar");
    loadingBar->setTag(0);
    loadingBar->loadTexture("loader2.png");
    loadingBar->setPercent(0);
    
    loadingBar->setPosition(Point(visibleSize.width / 2.0f, visibleSize.height / 2.0f + loadingBar->getContentSize().height / 4.0f));
    this->addChild(loadingBar, 3);
    
    auto Label = Label::createWithTTF( "Loading please wait", "calebasse.ttf", visibleSize.height * 0.04 );
    Label->setColor( Color3B::WHITE );
    Label->setPosition( Point(  visibleSize.width / 2.0f, visibleSize.height / 2.0f - barSprite2->getContentSize().height / 2.0f) );
    this->addChild( Label, 3);
    
    this->schedule(schedule_selector(SplashScreen::UpdateProgress),0.025f);
    //this->scheduleUpdate( );
    return true;
}

void SplashScreen::UpdateProgress(float dt)
{
    counter++;
    if (counter > 100)
    {
        this->scheduleOnce( schedule_selector( SplashScreen::GoToMainMenuScene ), 0 );

        unschedule(schedule_selector(SplashScreen::UpdateProgress));
    }
    loadingBar->setPercent(counter);
}
void SplashScreen::GoToMainMenuScene( float dt )
{
auto scene = MainMenu::createScene();
Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
}