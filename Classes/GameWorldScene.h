#ifndef __GAMEWORLD_SCENE_H__
#define __GAMEWORLD_SCENE_H__


#include "cocos2d.h"

#include "Definitions.h"
#include "Hero.h"
#include "Enemy.h"
#include "Coin.h"

#include "GameOverScene.h"

#include "Parallax/CCParallaxScrollNode.h"
#include "Parallax/CCParallaxScrollOffset.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "AdmobHelper.h"
#endif

class GameWorld : public cocos2d::LayerColor
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();
    
    cocos2d::Sprite *bg2;
    cocos2d::Sprite *bg2x;
    
    float screenWidth;
    float screenHeight;
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
	CCParallaxScrollNode *parallax;
	CCParallaxScrollNode *ground;
    Hero *hero;
    Enemy enemy;
    Coin coin;
    
    unsigned int score;
    unsigned int life;
    unsigned int coins;
    //unsigned int speed;
    unsigned int counter;
    unsigned int spawnNum;
    unsigned int coinSpawnNum;
    cocos2d::Label *scoreLabel;
    cocos2d::Label *scoreLabel2;
    cocos2d::Label *scoreLabel3;
    
    
	void update( float dt );
    void SpawnEnemy(float dt);
    void SpawnCoin(float dt);
    void MagnetCheck(float dt);
    void magnet(Node *pNode);
    
    void ClearCoin(Node *pNode);
    void MagnetCallback(Node *pNode);
    void menuCloseCallback(cocos2d::Ref* pSender);
    
	bool onTouchBegan( cocos2d::Touch *touch, cocos2d::Event *event );
    
    float screenPosition;
    float screenSpeed;
    
    MenuItemToggle* pp_toggle_item;
    void onKeyReleased( cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event );
    bool isPaused;
    bool isPopped;
    bool isMuted;
    UserDefault *def;
    void pauseNodeAndDescendants(Node *pNode);
    void resumeNodeAndDescendants(Node *pNode);
    void pauseCallback(cocos2d::Ref* pSender);
    void SoundCallback(cocos2d::Ref* pSender2);
    void quitPopUpLayer();
    void changeColor(float dt);
    void buttonCallback(cocos2d::Node *pNode);
    
    
    // implement the "static create()" method manually
    CREATE_FUNC(GameWorld);
private:
	cocos2d::PhysicsWorld *sceneWorld;
    void SetPhysicsWorld( cocos2d::PhysicsWorld *world ) { sceneWorld = world; };
	bool onContactBegin (cocos2d::PhysicsContact &contact);
};

#endif // __HELLOWORLD_SCENE_H__
