#ifndef __HELP_SCENE_H__
#define __HELP_SCENE_H__

#include "cocos2d.h"
#include "PopupLayer.h"
#include "Parallax/CCParallaxScrollNode.h"
#include "Parallax/CCParallaxScrollOffset.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "AdmobHelper.h"
#endif

class Help : public cocos2d::LayerColor
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    void fbCallback(Ref* pSender);
    void GoToGameScene(Ref *sender);
    void onKeyReleased( cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event );
    
    float screenWidth;
    float screenHeight;
    bool isPopped;
    void quitPopUpLayer();
    void buttonCallback(cocos2d::Node *pNode);
    // implement the "static create()" method manually
    CREATE_FUNC(Help);
};

#endif // __HELP_SCENE_H__
