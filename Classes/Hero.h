#ifndef __HERO_H__
#define __HERO_H__

#include "cocos2d.h"
#include "enemy.h"
#include "MyBodyParser.h"

class Hero
{
public:
    Hero( cocos2d::Layer *layer );
    

    void JumpLeft();
    void RotateJumpLeft();
    void JumpRight();
    void RotateJumpRight();
    void JumpCallback();
    void Invisible();
    void InvisibleCallback();
    void Magnet();
    void MagnetCallback();
    void CollideInvisible();
    void CollideInvisibleCallback();
    
    //void StopFlying( ) { isFalling = true; }
	cocos2d::Sprite *hero;
    cocos2d::Sprite *lane;
	cocos2d::PhysicsBody *heroBody;
	cocos2d::Animate *animate;
    cocos2d::JumpTo* jumpTo;
    cocos2d::Sprite *dummyHero;
    cocos2d::PhysicsBody *dummyHeroBody;
    
    
    float screenWidth;
    float screenHeight;
    
    float laneWidth;
    float laneHeight;
    bool isInvi;
    bool isJump;
    bool isMagnet;
    enum class LanePosition {
		ONE,
        TWO,
        THREE,
        FOUR,
        FIVE
	};
    LanePosition Position;
    
private:
    cocos2d::Size visibleSize;
    cocos2d::Vec2 origin;
    
    
    
    //bool isFalling;
    
};

#endif // __BIRD_H__
