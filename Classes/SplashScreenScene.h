#ifndef __SPLASHSCREEN_SCENE_H__
#define __SPLASHSCREEN_SCENE_H__

#include "cocos2d.h"

#include "ui/CocosGUI.h"

//#include "cocos-ext.h"
//using namespace cocos2d;
using namespace cocos2d::ui;
//using namespace cocos2d::extension;

class SplashScreen : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    LoadingBar* loadingBar;
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    void GoToMainMenuScene( float dt );
    //void update( float dt );
    
    void UpdateProgress(float dt);
	unsigned int counter;
    // implement the "static create()" method manually
    CREATE_FUNC(SplashScreen);
};

#endif // __HELLOWORLD_SCENE_H__
