#include "GameWorldScene.h"

USING_NS_CC;


Scene* GameWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::createWithPhysics( );
    //scene->getPhysicsWorld( )->setDebugDrawMask( PhysicsWorld::DEBUGDRAW_ALL );
    
    // 'layer' is an autorelease object
    auto layer = GameWorld::create();
	//SET GRAVITY
    scene->getPhysicsWorld()->setGravity(Vect(0, 0));

	layer->SetPhysicsWorld( scene->getPhysicsWorld( ) );
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor(Color4B(255,255,255,255)) )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    screenPosition = -720;

    //Set Resolution
    screenWidth = visibleSize.width;
    screenHeight = visibleSize.height;
    
    auto closeItem = MenuItemImage::create("quit.png",
                                           "quit.png",
                                           CC_CALLBACK_1(GameWorld::menuCloseCallback, this));
    closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));
    // create menu, it's an autorelease object
    
    auto closeMenu = Menu::create(closeItem, NULL);
    closeMenu->setPosition(Vec2::ZERO);
    this->addChild(closeMenu, CLOSE_BUTTON_LAYER);
    
    isPopped = false;
    isPaused = false;
    
    auto image1 = MenuItemImage::create("sound_on_toggle.png", "sound_on_toggle.png", NULL, NULL );
    auto image2 = MenuItemImage::create("sound_off_toggle.png", "sound_off_toggle.png", NULL, NULL );
    
    auto toggle_item = MenuItemToggle::createWithCallback(CC_CALLBACK_1(GameWorld::SoundCallback, this), image1 , image2, NULL);;
    toggle_item->setPosition(Vec2(origin.x + visibleSize.width - toggle_item->getContentSize().width/2 ,
                                  origin.y + visibleSize.height - toggle_item->getContentSize().height/2));
    auto soundMenu = Menu::create( toggle_item, NULL );
    soundMenu->setPosition(Vec2::ZERO);
    this->addChild(soundMenu, 100);
    
    auto play = MenuItemImage::create("play_toggle.png", "play_toggle.png", NULL, NULL );
    auto pause = MenuItemImage::create("pause_toggle.png", "pause_toggle.png", NULL, NULL );
    pp_toggle_item = MenuItemToggle::createWithCallback(CC_CALLBACK_1(GameWorld::pauseCallback, this), play , pause, NULL);;
    //pp_toggle_item->setPosition(Vec2(origin.x + pp_toggle_item->getContentSize().width/2 ,
    //                          origin.y + visibleSize.height - pp_toggle_item->getContentSize().height/2));
    pp_toggle_item->setPosition(Vec2(origin.x + visibleSize.width - toggle_item->getContentSize().width*2 ,
                                     origin.y + visibleSize.height - toggle_item->getContentSize().height/2));
    auto ppMenu = Menu::create(pp_toggle_item, NULL);
    ppMenu->setPosition(Vec2::ZERO);
    this->addChild(ppMenu, 100);
    
    auto edgeBody = PhysicsBody::createEdgeBox( visibleSize, PHYSICSBODY_MATERIAL_DEFAULT, 5 );
    edgeBody->setCollisionBitmask( OBSTACLE_COLLISION_BITMASK );
    edgeBody->setContactTestBitmask( true );
    auto edgeNode = Node::create();
    edgeNode->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y ) );
    edgeNode->setPhysicsBody( edgeBody );
    //this->addChild( edgeNode );
    
    parallax = CCParallaxScrollNode::create();
	auto bg1 = Sprite::create("bg1.png");
	bg2 = Sprite::create("bg2.png");
    auto bg3 = Sprite::create("aud1.png");
    auto bg4 = Sprite::create("aud2.png");
	auto bg1x = Sprite::create("bg1.png");
    bg2x = Sprite::create("bg2.png");
    auto bg3x = Sprite::create("aud1.png");
    auto bg4x = Sprite::create("aud2.png");
    
    
    auto stand = Sprite::create("stand.png");

	//auto groundBody = PhysicsBody::createBox( bg1->getContentSize( ) );
    //groundBody->setCollisionBitmask( OBSTACLE_COLLISION_BITMASK );
    //groundBody->setContactTestBitmask( true );
	//groundBody->setDynamic( false );
    //ground->setPhysicsBody( groundBody );
    
	//ground->addInfiniteScrollXWithZ(PARALLAX_GROUND_LAYER, Point(0.7,0), Point(0,0), bg1, bg1x, NULL);
	//parallax->addInfiniteScrollXWithZ(BG1_LAYER, Point(0.6,0), Point(0,bg1->getContentSize().height/2), bg2, bg2x, NULL);
    
	parallax->addInfiniteScrollYWithZ(BG1_LAYER, Point(0,0.8), Point((screenWidth - bg2->getContentSize().width)/2,0), bg2, bg2x, NULL);
    parallax->addInfiniteScrollYWithZ(BG2_LAYER, Point(0,+0.4), Point((screenWidth - bg1->getContentSize().width)/2,0), bg1, bg1x, NULL);
    parallax->addInfiniteScrollYWithZ(BG2_LAYER +1, Point(0,+0.4), Point((screenWidth - stand->getContentSize().width)/2 - bg3->getContentSize().width,0), bg3, bg3x, NULL);
    parallax->addInfiniteScrollYWithZ(BG2_LAYER +1, Point(0,+0.4), Point((screenWidth + stand->getContentSize().width)/2,0), bg4, bg4x, NULL);
    
	this->addChild(parallax,PARALLAX_WORLD_LAYER);
	//this->addChild(ground,PARALLAX_GROUND_LAYER);
    
    //auto backgroundSprite = Sprite::create( "bg2.png" );
    //backgroundSprite->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y) );
    //this->addChild( backgroundSprite, PARALLAX_WORLD_LAYER+2);
    
    hero = new Hero( this );
    
    //CONTACT LISTENER
    auto contactListener = EventListenerPhysicsContact::create( );
    contactListener->onContactBegin = CC_CALLBACK_1( GameWorld::onContactBegin, this );
    Director::getInstance( )->getEventDispatcher( )->addEventListenerWithSceneGraphPriority( contactListener, this );
    
    //TOUCH LISTENER
	auto touchListener = EventListenerTouchOneByOne::create( );
    touchListener->setSwallowTouches( true );
    touchListener->onTouchBegan = CC_CALLBACK_2( GameWorld::onTouchBegan, this );
    Director::getInstance( )->getEventDispatcher( )->addEventListenerWithSceneGraphPriority( touchListener, this );
    
    //MUTE
    def = UserDefault::getInstance( );
    isMuted = def->getBoolForKey("Mute");
    if(isMuted){
        CocosDenshion::SimpleAudioEngine::getInstance( )->setBackgroundMusicVolume(0);
        CocosDenshion::SimpleAudioEngine::getInstance( )->setEffectsVolume(0);
        toggle_item->setSelectedIndex(1);
    }
    
    //SCORE
    score = 0;
    coins=0;
    life=2;
    counter = 0;
    spawnNum = 0;
    coinSpawnNum = 0;
    //speed = 25;
    String *tempScore = String::createWithFormat( "Dist - %i m", score );
    //scoreLabel = LabelTTF::create(tempScore->getCString( ), "Arial", 22);
    scoreLabel = Label::createWithTTF( tempScore->getCString( ), "SuperMario256.ttf", visibleSize.height * SCORE_FONT_SIZE );
    scoreLabel->setColor( Color3B::GRAY );
    scoreLabel->enableOutline(Color4B::BLACK,2);
    scoreLabel->setPosition( Point(  screenWidth-screenWidth/6, screenHeight - screenHeight/10) );
    this->addChild( scoreLabel, 50);
    
    String *tempCoins = String::createWithFormat( "Coins - %i", coins );
    
    scoreLabel2 = Label::createWithTTF( tempCoins->getCString( ), "SuperMario256.ttf", visibleSize.height * SCORE_FONT_SIZE );
    scoreLabel2->setColor( Color3B::YELLOW );
    scoreLabel2->enableOutline(Color4B::BLACK,2);
    //scoreLabel2 = LabelTTF::create(tempCoins->getCString( ), "Arial", 22);
    scoreLabel2->setPosition( Point(  screenWidth/2, screenHeight - screenHeight/10) );
    this->addChild( scoreLabel2, 22);
    
    String *tempLife = String::createWithFormat( "Life - %i", life );
    
    scoreLabel3 = Label::createWithTTF( tempLife->getCString( ), "SuperMario256.ttf", visibleSize.height * SCORE_FONT_SIZE );
    scoreLabel3->setColor( Color3B::ORANGE );
    scoreLabel3->enableOutline(Color4B::BLACK,2);
    //scoreLabel2 = LabelTTF::create(tempCoins->getCString( ), "Arial", 22);
    scoreLabel3->setPosition( Point(  screenWidth/6, screenHeight - screenHeight/10) );
    this->addChild( scoreLabel3, 22);
    
    this->schedule(schedule_selector(GameWorld::SpawnCoin),  1 );
    this->schedule(schedule_selector(GameWorld::SpawnEnemy),  2 ); //3
    this->schedule(schedule_selector(GameWorld::MagnetCheck),  1 ); //3
    this->scheduleUpdate( );
    
    return true;
}
void GameWorld::SpawnEnemy(float dt)
{
    auto random = CCRANDOM_0_1( );
    if ( random < 0.75 )
    {
    enemy.SpawnEnemy( this ,spawnNum++);
    }else{
        auto random2 = CCRANDOM_0_1( );
        if (random2 < 0.25 ){
            enemy.SpawnObstacle( this ,spawnNum++);
        }else if(random2>0.25 && random2<=0.50){
            enemy.SpawnCutter( this ,spawnNum++);
        }else if(random2>0.50 && random2 <=0.75){
            enemy.SpawnBlender( this ,spawnNum++);
        }else if(random2>0.75){
            enemy.SpawnFlamer( this ,spawnNum++);
        }
    }
    
    //coin.SpawnPipe2( this);
}

void GameWorld::SpawnCoin(float dt)
{
    if(coinSpawnNum%20 || coinSpawnNum == 0){
        coin.SpawnPipe2( this,coinSpawnNum++);
    }else{
        auto random = CCRANDOM_0_1( );
        if (random < 0.40 ){
            coin.SpawnMagnet( this,coinSpawnNum++);
        }else if(random >=0.40 && random < 0.80){
            coin.SpawnInvi( this,coinSpawnNum++);
        }else if (random>=0.80){
            coin.SpawnLife( this,coinSpawnNum++);
        }
    }
    //CCLOG("%d coin",coinSpawnNum);
}
void GameWorld::MagnetCheck(float dt)
{
    if(hero->isMagnet){
        magnet(this);
    }
}
bool GameWorld::onContactBegin (cocos2d::PhysicsContact &contact){
    //COLLISION DETECTION
    PhysicsBody *a = contact.getShapeA( )->getBody();
    PhysicsBody *b = contact.getShapeB( )->getBody();
    
    if ( ( BIRD_COLLISION_BITMASK == a->getCollisionBitmask( ) && OBSTACLE_COLLISION_BITMASK == b->getCollisionBitmask() ) || ( BIRD_COLLISION_BITMASK == b->getCollisionBitmask( ) && OBSTACLE_COLLISION_BITMASK == a->getCollisionBitmask() ) )
    {
        if(OBSTACLE_COLLISION_BITMASK == b->getCollisionBitmask()){
            b->setEnable(false);
        }else{
            a->setEnable(false);
        }
        if(hero->isInvi==false)
        {
            CocosDenshion::SimpleAudioEngine::getInstance( )->playEffect( "sounds/Hit.ogg" );
            if (life!=0){
            hero->CollideInvisible();
            life--;
            __String *tempLife = __String::createWithFormat( "Life - %i", life );
            scoreLabel3->setString( tempLife->getCString( ) );
            //CCLOG("%d",life);
            }else{
            //GAMEOVER
            auto scene = GameOver::createScene(score,coins);
            Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
            }
        }
    }else if ( ( BIRD_COLLISION_BITMASK == a->getCollisionBitmask( ) && MOVING_OBSTACLE_COLLISION_BITMASK == b->getCollisionBitmask() ) || ( BIRD_COLLISION_BITMASK == b->getCollisionBitmask( ) && MOVING_OBSTACLE_COLLISION_BITMASK == a->getCollisionBitmask() ) )
    {
        if(MOVING_OBSTACLE_COLLISION_BITMASK == b->getCollisionBitmask()){
            b->setEnable(false);
        }else{
            a->setEnable(false);
        }
        if(hero->isInvi==false)
        {
            CocosDenshion::SimpleAudioEngine::getInstance( )->playEffect( "sounds/Hit.ogg" );
            if (life!=0){
                hero->CollideInvisible();
                life--;
                __String *tempLife = __String::createWithFormat( "Life - %i", life );
                scoreLabel3->setString( tempLife->getCString( ) );
                CCLOG("%d",life);
            }else{
                //GAMEOVER
                auto scene = GameOver::createScene(score,coins);
                Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
            }
        }
    }else if ( ( BIRD_COLLISION_BITMASK == a->getCollisionBitmask( ) && COIN_COLLISION_BITMASK == b->getCollisionBitmask() ) || ( BIRD_COLLISION_BITMASK == b->getCollisionBitmask( ) && COIN_COLLISION_BITMASK == a->getCollisionBitmask() ) )
    {
        //CCLOG("%d A, %d B",a->getTag(),b->getTag());
        unsigned int tag1 = a->getTag();
        unsigned int tag2 = b->getTag();
        if(tag1>0)
        {
            this->removeChildByTag(tag1);
        }
        if(tag2>0)
        {
            this->removeChildByTag(tag2);
        }
        if(tag1==0 && tag2==0)
        {
            this->removeChildByTag(0);
        }
        
        CCLOG("Scored");
        //SCORED
        
        
        coins++;
        __String *tempCoins = __String::createWithFormat( "Coins - %i", coins );
        scoreLabel2->setString( tempCoins->getCString( ) );
    }else if ( ( COIN_COLLISION_BITMASK == a->getCollisionBitmask( ) && OBSTACLE_COLLISION_BITMASK == b->getCollisionBitmask() ) || ( COIN_COLLISION_BITMASK == b->getCollisionBitmask( ) && OBSTACLE_COLLISION_BITMASK == a->getCollisionBitmask() ) )
    {
        //CCLOG("%d A, %d B",a->getTag(),b->getTag());
        unsigned int tag1 = a->getTag();
        unsigned int tag2 = b->getTag();
        if(tag1>0)
        {
            /*auto m_emitter = CCParticleExplosion::create();
             m_emitter->setPosition(a->getPosition());
             m_emitter->setLife(0.5);
             m_emitter->setSpeed(1.0);
             m_emitter->setSpeedVar(50.0);
             this->addChild(m_emitter,1);*/
            this->removeChildByTag(tag1);
        }
        if(tag2>0)
        {
            this->removeChildByTag(tag2);
        }
        if(tag1==0 && tag2==0)
        {
            this->removeChildByTag(0);
        }
    }else if ( ( BIRD_COLLISION_BITMASK == a->getCollisionBitmask( ) && MAGNET_COLLISION_BITMASK == b->getCollisionBitmask() ) || ( BIRD_COLLISION_BITMASK == b->getCollisionBitmask( ) && MAGNET_COLLISION_BITMASK == a->getCollisionBitmask() ) )
    {
        //CCLOG("%d A, %d B",a->getTag(),b->getTag());
        unsigned int tag1 = a->getTag();
        unsigned int tag2 = b->getTag();
        if(tag1>0)
        {
            this->removeChildByTag(tag1);
        }
        if(tag2>0)
        {
            this->removeChildByTag(tag2);
        }
        if(tag1==0 && tag2==0)
        {
            this->removeChildByTag(0);
        }
        hero->Magnet();
    }else if ( ( BIRD_COLLISION_BITMASK == a->getCollisionBitmask( ) && INVI_COLLISION_BITMASK == b->getCollisionBitmask() ) || ( BIRD_COLLISION_BITMASK == b->getCollisionBitmask( ) && INVI_COLLISION_BITMASK == a->getCollisionBitmask() ) )
    {
        //CCLOG("%d A, %d B",a->getTag(),b->getTag());
        unsigned int tag1 = a->getTag();
        unsigned int tag2 = b->getTag();
        if(tag1>0)
        {
            this->removeChildByTag(tag1);
        }
        if(tag2>0)
        {
            this->removeChildByTag(tag2);
        }
        if(tag1==0 && tag2==0)
        {
            this->removeChildByTag(0);
        }
        hero->Invisible();
    }else if ( ( BIRD_COLLISION_BITMASK == a->getCollisionBitmask( ) && LIFE_COLLISION_BITMASK == b->getCollisionBitmask() ) || ( BIRD_COLLISION_BITMASK == b->getCollisionBitmask( ) && LIFE_COLLISION_BITMASK == a->getCollisionBitmask() ) )
    {
        //CCLOG("%d A, %d B",a->getTag(),b->getTag());
        unsigned int tag1 = a->getTag();
        unsigned int tag2 = b->getTag();
        if(tag1>0)
        {
            this->removeChildByTag(tag1);
        }
        if(tag2>0)
        {
            this->removeChildByTag(tag2);
        }
        if(tag1==0 && tag2==0)
        {
            this->removeChildByTag(0);
        }
        if(life<2)
        {
        life++;
        __String *tempLife = __String::createWithFormat( "Life - %i", life );
        scoreLabel3->setString( tempLife->getCString( ) );
        }
    }
    return true;
}
bool GameWorld::onTouchBegan( cocos2d::Touch *touch, cocos2d::Event *event )
{
    Point touchPoint = Point(touch->getLocationInView().x, touch->getLocationInView().y);
    if(touchPoint.x < screenWidth/2)
    {
        auto random = CCRANDOM_0_1();
        if(random>0.25)
        hero->JumpLeft();
        else
        hero->RotateJumpLeft();
    }else{
        auto random = CCRANDOM_0_1 ();
        if(random>0.25)
        hero->JumpRight();
        else
        hero->RotateJumpRight();
    }
    return true;
}
void GameWorld::update( float dt )
{
	//PARALLAX
    //hero->Stay();
    if(score%1000 == 0 && score !=0){
        this->scheduleOnce( schedule_selector( GameWorld::changeColor ), 1 );
    }
    ClearCoin(this);
    parallax->updateWithVelocity(Point(0,-screenWidth/100),dt);
    score++;
    __String *tempScore = __String::createWithFormat( "Dist - %i m", score );
    scoreLabel->setString( tempScore->getCString( ) );
    //getName("coin")->runAction(Follow::create(hero->hero));
    //getChildByName("coin")->runAction(Follow::create(hero->hero));
}
void GameWorld::changeColor(float dt)
{
    auto randomC = CCRANDOM_0_1( );
    if ( randomC < 0.25 )
    {
        auto Tint = TintTo::create(10, 255, 0, 255);
        auto Tint2 = TintTo::create(10, 255, 0, 255);
        this->bg2->runAction(Tint);
        this->bg2x->runAction(Tint2);
        //CCLOG("GREEN");
    }else if ( randomC >= 0.25 && randomC < 0.50)
    {
        auto Tint = TintTo::create(10, 0, 255, 255);
        auto Tint2 = TintTo::create(10, 0, 255, 255);
        this->bg2->runAction(Tint);
        this->bg2x->runAction(Tint2);
        //CCLOG("RED");
    }else if ( randomC >= 0.50 && randomC < 0.75 )
    {
        auto Tint = TintTo::create(10, 255, 255, 0);
        auto Tint2 = TintTo::create(10, 255, 255, 0);
        this->bg2->runAction(Tint);
        this->bg2x->runAction(Tint2);
        //CCLOG("BLUE");
    }else if ( randomC >= 0.75 )
    {
        auto Tint = TintTo::create(10, 255, 255, 255);
        auto Tint2 = TintTo::create(10, 255, 255, 255);
        this->bg2->runAction(Tint);
        this->bg2x->runAction(Tint2);
        //CCLOG("WHITE");
    }
}
void GameWorld::magnet(Node *pNode)
{

        for(const auto &child : pNode->getChildren())
        {
            //this->resumeNodeAndDescendants(child);
            //if(child == pNode->getChildByName("coin")){
                if(child == pNode->getChildByName("coin")){
                child->getPhysicsBody()->setEnable(false);
                auto  move = MoveTo::create(0.2,Point(hero->hero->getPositionX(), hero->hero->getPositionY()));  //10 Blinks in 5 secs
                auto callback = CallFunc::create( [this]() {
                    MagnetCallback(this);
                });
                auto sequence = Sequence::create(move, callback, NULL);
                child->runAction(sequence);
                //child->runAction(Follow::create(hero->hero,Rect(0, 0, screenWidth, screenHeight)));
            }
        }
        //getChildByName("coin")->runAction(MoveTo::create(0.1,Point(hero->hero->getPositionX(), hero->hero->getPositionY())));

}
void GameWorld::MagnetCallback(Node *pNode)
{
    pNode->removeChildByName("coin");
    coins++;
    CCLOG("%d COINSSSSS",coins);
    __String *tempCoins = __String::createWithFormat( "Coins - %i", coins );
    scoreLabel2->setString( tempCoins->getCString( ) );
}

void GameWorld::ClearCoin(Node *pNode)
{
    for(const auto &child : pNode->getChildren())
    {
        if(child == pNode->getChildByName("coin")){
            if(child->getPositionY() <0)
                pNode->removeChild(child);
        }
    }
}
void GameWorld::pauseNodeAndDescendants(Node *pNode)
{
    pNode->pause();
    for(const auto &child : pNode->getChildren())
    {
        //if(pNode->getChildren() != this->ppMenu)
        this->pauseNodeAndDescendants(child);
    }
}
void GameWorld::resumeNodeAndDescendants(Node *pNode)
{
    pNode->resume();
    for(const auto &child : pNode->getChildren())
    {
        this->resumeNodeAndDescendants(child);
    }
}
void GameWorld::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
    MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif
    if(!isPopped){
    quitPopUpLayer();
    this->pause();
    this->getChildByName("hero")->getPhysicsBody()->setEnable(false);
    pauseNodeAndDescendants(this);
    isPaused=true;
    }
}
void GameWorld::pauseCallback(Ref* pSender)
{
    
    auto toggleItem = dynamic_cast<MenuItemToggle*>(pSender);
    if (toggleItem->getSelectedIndex() == 1 ) {
        this->pause();
        //this->pauseSchedulerAndActions();
        this->getChildByName("hero")->getPhysicsBody()->setEnable(false);
        pauseNodeAndDescendants(this);
        isPaused=true;
        //getChildByName("hero")->pause();
        /*if(spawnNum>0)
         {
         pipe.topPipe->pause();
         pipe.bottomPipe->pause();
         pipe.pointNode->pause();
         }*/
    }else if ( toggleItem->getSelectedIndex() == 0 ) {
        this->resume();
        this->getChildByName("hero")->getPhysicsBody()->setEnable(true);
        resumeNodeAndDescendants(this);
        isPaused=false;
        //getChildByName("hero")->resume();
        /*if(spawnNum>0)
         {
         pipe.topPipe->resume();
         pipe.bottomPipe->resume();
         pipe.pointNode->resume();
         }*/
    }
}

void GameWorld::SoundCallback(Ref* pSender2)
{
    auto toggleItem = dynamic_cast<MenuItemToggle*>(pSender2);
    if (toggleItem->getSelectedIndex() == 1 ) {
        CocosDenshion::SimpleAudioEngine::getInstance( )->setBackgroundMusicVolume(0);
        CocosDenshion::SimpleAudioEngine::getInstance( )->setEffectsVolume(0);
        isMuted=true;
        //CCLOG("Muted");
    }
    
    else if ( toggleItem->getSelectedIndex() == 0 ) {
        CocosDenshion::SimpleAudioEngine::getInstance( )->setBackgroundMusicVolume(1);
        CocosDenshion::SimpleAudioEngine::getInstance( )->setEffectsVolume(1);
        isMuted=false;
        //CCLOG("Unmuted");
    }
    def->setBoolForKey("Mute", isMuted);
    def->flush( );
}

void GameWorld::onKeyReleased( cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event )
{
    if(!isPopped)
    {
        quitPopUpLayer();
        this->pause();
        this->getChildByName("hero")->getPhysicsBody()->setEnable(false);
        pauseNodeAndDescendants(this);
        isPaused=true;
    }
    // CallFuncN(this, callfunc_selector(GameWorld::buttonCallback));
    //Director::getInstance()->end();
}
void GameWorld::quitPopUpLayer(){
    isPopped = true;
    PopupLayer* pl = PopupLayer::create("pop_panel.png");
    pl->setContentSize(CCSizeMake(400, 150));
    pl->setTitle("");
    pl->setContentText("Do you want to quit playing ?", 20, 60, 250);
    pl->setCallbackFunc(this, callfuncN_selector(GameWorld::buttonCallback));
    pl->addButton("pop_button.png", "pop_button.png", "No", 0);
    pl->addButton("pop_button.png", "pop_button.png", "Yes", 1);
    this->addChild(pl,20);
}
void GameWorld::buttonCallback(cocos2d::Node *pNode){
    //CCLog("button call back. tag: %d", pNode->getTag());
    if(pNode->getTag() == 0)
    {
        isPopped = false;
        this->resume();
        this->getChildByName("hero")->getPhysicsBody()->setEnable(true);
        resumeNodeAndDescendants(this);
    }else{
        Director::getInstance()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        exit(0);
#endif
    }
}
