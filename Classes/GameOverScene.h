#ifndef __GAMEOVER_SCENE_H__
#define __GAMEOVER_SCENE_H__

#include "cocos2d.h"
#include "Parallax/CCParallaxScrollNode.h"
#include "Parallax/CCParallaxScrollOffset.h"
#include "PopupLayer.h"

class GameOver : public cocos2d::LayerColor
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene(unsigned int tempScore, unsigned int tempSpeed);
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    int totalGameScore;
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);

    void onKeyReleased( cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event );
    bool isPopped;
    void quitPopUpLayer();
    void buttonCallback(cocos2d::Node *pNode);
    
    // implement the "static create()" method manually
    CREATE_FUNC(GameOver);

    void GoToGameScene(Ref *sender);

};

#endif // __HELLOWORLD_SCENE_H__
