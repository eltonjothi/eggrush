#include "Enemy.h"
#include "Definitions.h"

USING_NS_CC;
unsigned int spawntag;
float speedInc;

Enemy::Enemy( )
{
    visibleSize = Director::getInstance( )->getVisibleSize( );
    origin = Director::getInstance( )->getVisibleOrigin( );
}

void Enemy::SpawnEnemy( cocos2d::Layer *layer , unsigned int spawnNum)
{
    visibleSize = Director::getInstance( )->getVisibleSize( );
    origin = Director::getInstance( )->getVisibleOrigin( );
    
    //Set Resolution
    screenWidth = visibleSize.width;
    screenHeight = visibleSize.height;
    
    auto lane = Sprite::create("bg2.png");
    laneWidth=lane->getContentSize().width;
    laneHeight=lane->getContentSize().height;
    
    float pipe_speed;
    if(visibleSize.height > LARGE_HEIGHT)
    {
        pipe_speed = 0.0037;
    }else{
        pipe_speed = 0.0075;
    }
    
    spawntag = spawnNum;
    
    if(spawntag%2){
		speedInc = spawntag * 0.1;
		//CCLOG("Speed Increased : %2f",speedInc);
	}

    //MyBodyParser::getInstance()->parseJsonFile("obstacleBody.json");
	//auto enemyEggBody = MyBodyParser::getInstance()->bodyFormJson(enemyEgg, "obs1");
    
    enemyEgg = Sprite::create( "egg.png" );
    enemyEgg2 = Sprite::create( "egg.png" );
    enemyEgg3 = Sprite::create( "egg.png" );
    coin = Sprite::create("coin.png");
    auto enemyEggBody = PhysicsBody::createCircle( enemyEgg->getContentSize( ).width / 4, PhysicsMaterial(0,0,0) );
    enemyEggBody->setDynamic( false );
    enemyEggBody->setCollisionBitmask( OBSTACLE_COLLISION_BITMASK );
    enemyEggBody->setContactTestBitmask( true );
    
    auto enemyEggBody2 = PhysicsBody::createCircle( enemyEgg2->getContentSize( ).width / 4, PhysicsMaterial(0,0,0) );
    enemyEggBody2->setDynamic( false );
    enemyEggBody2->setCollisionBitmask( OBSTACLE_COLLISION_BITMASK );
    enemyEggBody2->setContactTestBitmask( true );
    
    auto enemyEggBody3 = PhysicsBody::createCircle( enemyEgg3->getContentSize( ).width / 4, PhysicsMaterial(0,0,0) );
    enemyEggBody3->setDynamic( false );
    enemyEggBody3->setCollisionBitmask( OBSTACLE_COLLISION_BITMASK );
    enemyEggBody3->setContactTestBitmask( true );
    
    auto coinBody = PhysicsBody::createCircle( coin->getContentSize( ).width / 4, PhysicsMaterial(0,0,0) );
    coinBody->setDynamic( false );
    coinBody->setCollisionBitmask( COIN_COLLISION_BITMASK );
    coinBody->setContactTestBitmask( true );

    
    enemyEgg->setPhysicsBody(enemyEggBody);
    enemyEgg2->setPhysicsBody(enemyEggBody2);
    enemyEgg3->setPhysicsBody(enemyEggBody3);
    coin->setPhysicsBody(coinBody);
    
    //CCLOG("%f, %f",laneWidth,screenWidth);
    //Position = LanePosition::ONE;
    //enemyEgg->setPosition( Point( visibleSize.width/2 - 0.4 * laneWidth , visibleSize.height - enemyEgg->getContentSize().height ) );
    //layer->addChild( enemyEgg, ENEMY_LAYER);
    //auto enemyEggAction = MoveTo::create( 5  , Vec2(enemyEgg->getPositionX() , 10 ) );
    //enemyEgg->runAction( enemyEggAction );
    //auto bottomPipeBody = PhysicsBody::createBox( bottomPipe->getContentSize( ) );

    SpriteFrameCache* cache = SpriteFrameCache::getInstance();
    cache->addSpriteFramesWithFile("eggrush_run.plist");

    Vector<SpriteFrame*> animFrames(3);
    char str[100] = {0};
    auto randomVeg = CCRANDOM_0_1( );
    if ( randomVeg < 0.07 )
    {
        for(int i = 1; i <=3; i++)
        {
            sprintf(str, "bet%d",i);
            SpriteFrame* frame = cache->getSpriteFrameByName( str );
            animFrames.pushBack(frame);
        }
    }else if( randomVeg >=0.07 && randomVeg < 0.14)
    {
        for(int i = 1; i <=3; i++)
        {
            sprintf(str, "bri%d",i);
            SpriteFrame* frame = cache->getSpriteFrameByName( str );
            animFrames.pushBack(frame);
        }
    }else if( randomVeg >=0.14 && randomVeg < 0.21)
    {
        for(int i = 1; i <=3; i++)
        {
            sprintf(str, "broc%d",i);
            SpriteFrame* frame = cache->getSpriteFrameByName( str );
            animFrames.pushBack(frame);
        }
    }else if( randomVeg >=0.21 && randomVeg < 0.28)
    {
        for(int i = 1; i <=3; i++)
        {
            sprintf(str, "cab%d",i);
            SpriteFrame* frame = cache->getSpriteFrameByName( str );
            animFrames.pushBack(frame);
        }
    }else if( randomVeg >=0.28 && randomVeg < 0.35)
    {
        for(int i = 1; i <=3; i++)
        {
            sprintf(str, "car%d",i);
            SpriteFrame* frame = cache->getSpriteFrameByName( str );
            animFrames.pushBack(frame);
        }
    }else if( randomVeg >=0.35 && randomVeg < 0.42)
    {
        for(int i = 1; i <=3; i++)
        {
            sprintf(str, "cuc%d",i);
            SpriteFrame* frame = cache->getSpriteFrameByName( str );
            animFrames.pushBack(frame);
        }
    }else if( randomVeg >=0.42 && randomVeg < 0.49)
    {
        for(int i = 1; i <=3; i++)
        {
            sprintf(str, "oni%d",i);
            SpriteFrame* frame = cache->getSpriteFrameByName( str );
            animFrames.pushBack(frame);
        }
    }else if( randomVeg >=0.49 && randomVeg < 0.56)
    {
        for(int i = 1; i <=3; i++)
        {
            sprintf(str, "pep%d",i);
            SpriteFrame* frame = cache->getSpriteFrameByName( str );
            animFrames.pushBack(frame);
        }
    }else if( randomVeg >=0.56 && randomVeg < 0.63)
    {
        for(int i = 1; i <=3; i++)
        {
            sprintf(str, "pop%d",i);
            SpriteFrame* frame = cache->getSpriteFrameByName( str );
            animFrames.pushBack(frame);
        }
    }else if( randomVeg >=0.63 && randomVeg < 0.70)
    {
        for(int i = 1; i <=3; i++)
        {
            sprintf(str, "pum%d",i);
            SpriteFrame* frame = cache->getSpriteFrameByName( str );
            animFrames.pushBack(frame);
        }
    }else if( randomVeg >=0.70 && randomVeg < 0.77)
    {
        for(int i = 1; i <=3; i++)
        {
            sprintf(str, "rad%d",i);
            SpriteFrame* frame = cache->getSpriteFrameByName( str );
            animFrames.pushBack(frame);
        }
    }else if( randomVeg >=0.77 && randomVeg < 0.84)
    {
        for(int i = 1; i <=3; i++)
        {
            sprintf(str, "spi%d",i);
            SpriteFrame* frame = cache->getSpriteFrameByName( str );
            animFrames.pushBack(frame);
        }
    }else if( randomVeg >=0.84 && randomVeg < 0.91)
    {
        for(int i = 1; i <=3; i++)
        {
            sprintf(str, "tom%d",i);
            SpriteFrame* frame = cache->getSpriteFrameByName( str );
            animFrames.pushBack(frame);
        }
    }else if( randomVeg >=0.91)
    {
        for(int i = 1; i <=3; i++)
        {
            sprintf(str, "ypep%d",i);
            SpriteFrame* frame = cache->getSpriteFrameByName( str );
            animFrames.pushBack(frame);
        }
    }
    auto animation = Animation::createWithSpriteFrames(animFrames, 0.05f);
    //animate = Animate::create(animation);
    enemyEgg->runAction(RepeatForever::create(Animate::create(animation)));
    enemyEgg2->runAction(RepeatForever::create(Animate::create(animation)));
    enemyEgg3->runAction(RepeatForever::create(Animate::create(animation)));
    
    
    auto random = CCRANDOM_0_1( );
    
    if ( random < 0.20 )
    {
        Position = LanePosition::ONE;
    }
    else if ( random >=0.20 && random < 0.40)
    {
        Position = LanePosition::TWO;
    }
    else if ( random >=0.40 && random < 0.60)
    {
        Position = LanePosition::THREE;
    }
    else if ( random >=0.60 && random < 0.80)
    {
        Position = LanePosition::FOUR;
    }
    else if ( random >=0.80)
    {
        Position = LanePosition::FIVE;
    }
    //CCLOG("%f",random);
    //auto enemyEggPosition = ( random * visibleSize.height ) + ( enemyEgg->getContentSize( ).height / 2 );
    auto random2 = CCRANDOM_0_1();
    if(random2<0.50)
    {
        if (Position == LanePosition::ONE)
        {
            enemyEgg->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
            layer->addChild( enemyEgg, ENEMY_LAYER);
            auto enemyEggAction = MoveTo::create( 4  , Point(screenWidth/2 - 0.4 * laneWidth , -visibleSize.height/2 ) );
            enemyEgg->runAction( enemyEggAction );
        }
        if (Position == LanePosition::TWO)
        {
            enemyEgg->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
            layer->addChild( enemyEgg, ENEMY_LAYER);
            auto enemyEggAction = MoveTo::create( 4 , Point( screenWidth/2 - 0.2 * laneWidth , -visibleSize.height/2 ) );
            enemyEgg->runAction( enemyEggAction );
        }
        if (Position == LanePosition::THREE)
        {
            enemyEgg->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
            layer->addChild( enemyEgg, ENEMY_LAYER);
            auto enemyEggAction = MoveTo::create( 4 , Point( screenWidth/2, -visibleSize.height/2 ) );
            enemyEgg->runAction( enemyEggAction );
        }
        if (Position == LanePosition::FOUR)
        {
            enemyEgg->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
            layer->addChild( enemyEgg, ENEMY_LAYER);
            auto enemyEggAction = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
            enemyEgg->runAction( enemyEggAction );
        }
        if (Position == LanePosition::FIVE)
        {
            enemyEgg->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
            layer->addChild( enemyEgg, ENEMY_LAYER);
            auto enemyEggAction = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
            enemyEgg->runAction( enemyEggAction );
        }
    }else if(random2>=0.50 && random2<0.80){
        //Spawn 2 eggs
        if (Position == LanePosition::ONE)
        {
            enemyEgg->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
            layer->addChild( enemyEgg, ENEMY_LAYER);
            auto enemyEggAction = MoveTo::create( 4  , Point(screenWidth/2 - 0.4 * laneWidth , -visibleSize.height/2 ) );
            enemyEgg->runAction( enemyEggAction );
            auto random3 = CCRANDOM_0_1();
            if(random3<0.25)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4 , Point( screenWidth/2 - 0.2 * laneWidth , -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
            }else if (random3 >=0.25 && random3 < 0.50)
            {
                enemyEgg2->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4 , Point( screenWidth/2, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
            }else if(random3 >=0.50 && random3 < 0.75)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
            }else if (random3 >= 0.75)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
            }
        }
        if (Position == LanePosition::TWO)
        {
            enemyEgg->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
            layer->addChild( enemyEgg, ENEMY_LAYER);
            auto enemyEggAction = MoveTo::create( 4 , Point( screenWidth/2 - 0.2 * laneWidth , -visibleSize.height/2 ) );
            enemyEgg->runAction( enemyEggAction );
            auto random3 = CCRANDOM_0_1();
            if(random3<0.25)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4 , Point( screenWidth/2 - 0.4 * laneWidth , -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
            }else if (random3 >=0.25 && random3 < 0.50)
            {
                enemyEgg2->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4 , Point( screenWidth/2, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
            }else if(random3 >=0.50 && random3 < 0.75)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
            }else if (random3 >= 0.75)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
            }
        }
        if (Position == LanePosition::THREE)
        {
            enemyEgg->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
            layer->addChild( enemyEgg, ENEMY_LAYER);
            auto enemyEggAction = MoveTo::create( 4 , Point( screenWidth/2, -visibleSize.height/2 ) );
            enemyEgg->runAction( enemyEggAction );
            auto random3 = CCRANDOM_0_1();
            if(random3<0.25)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4 , Point( screenWidth/2 - 0.4 * laneWidth , -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
            }else if (random3 >=0.25 && random3 < 0.50)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4 , Point( screenWidth/2 - 0.2 * laneWidth , -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
            }else if(random3 >=0.50 && random3 < 0.75)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
            }else if (random3 >= 0.75)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
            }
        }
        if (Position == LanePosition::FOUR)
        {
            enemyEgg->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
            layer->addChild( enemyEgg, ENEMY_LAYER);
            auto enemyEggAction = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
            enemyEgg->runAction( enemyEggAction );
            auto random3 = CCRANDOM_0_1();
            if(random3<0.25)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4 , Point( screenWidth/2 - 0.4 * laneWidth , -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
            }else if (random3 >=0.25 && random3 < 0.50)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4 , Point( screenWidth/2 - 0.2 * laneWidth , -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
            }else if(random3 >=0.50 && random3 < 0.75)
            {
                enemyEgg2->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4, Point( screenWidth/2, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
            }else if (random3 >= 0.75)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
            }
        }
        if (Position == LanePosition::FIVE)
        {
            enemyEgg->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
            layer->addChild( enemyEgg, ENEMY_LAYER);
            auto enemyEggAction = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
            enemyEgg->runAction( enemyEggAction );
            auto random3 = CCRANDOM_0_1();
            if(random3<0.25)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4 , Point( screenWidth/2 - 0.4 * laneWidth , -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
            }else if (random3 >=0.25 && random3 < 0.50)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4 , Point( screenWidth/2 - 0.2 * laneWidth , -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
            }else if(random3 >=0.50 && random3 < 0.75)
            {
                enemyEgg2->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4, Point( screenWidth/2, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
            }else if (random3 >= 0.75)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4  , Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
            }
        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    else{
        //Spawn 3 eggs
        if (Position == LanePosition::ONE)
        {
            enemyEgg->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
            layer->addChild( enemyEgg, ENEMY_LAYER);
            auto enemyEggAction = MoveTo::create( 4  , Point(screenWidth/2 - 0.4 * laneWidth , -visibleSize.height/2 ) );
            enemyEgg->runAction( enemyEggAction );
            auto random3 = CCRANDOM_0_1();
            if(random3<0.25)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4 , Point( screenWidth/2 - 0.2 * laneWidth , -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
                    auto random4 = CCRANDOM_0_1();
                    if(random4<0.33)
                    {
                        enemyEgg3->setPosition( Point( screenWidth/2, visibleSize.height + enemyEgg3->getContentSize().height ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4 , Point( screenWidth/2, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.33 && random4<0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }
            }else if (random3 >=0.25 && random3 < 0.50)
            {
                enemyEgg2->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4 , Point( screenWidth/2, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
                    auto random4 = CCRANDOM_0_1();
                    if(random4<0.33)
                    {
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4 , Point( screenWidth/2 - 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.33 && random4<0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }
            }else if(random3 >=0.50 && random3 < 0.75)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
                    auto random4 = CCRANDOM_0_1();
                    if(random4<0.33)
                    {
                        enemyEgg3->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4 , Point( screenWidth/2, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.33 && random4<0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4, Point( screenWidth/2 - 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }
                
            }else if (random3 >= 0.75)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
                    auto random4 = CCRANDOM_0_1();
                    if(random4<0.33)
                    {
                        enemyEgg3->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4 , Point( screenWidth/2, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.33 && random4<0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4, Point( screenWidth/2 - 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4  , Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }
            }
        }
        if (Position == LanePosition::TWO)
        {
            enemyEgg->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
            layer->addChild( enemyEgg, ENEMY_LAYER);
            auto enemyEggAction = MoveTo::create( 4 , Point( screenWidth/2 - 0.2 * laneWidth , -visibleSize.height/2 ) );
            enemyEgg->runAction( enemyEggAction );
            auto random3 = CCRANDOM_0_1();
            if(random3<0.25)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4 , Point( screenWidth/2 - 0.4 * laneWidth , -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
                    auto random4 = CCRANDOM_0_1();
                    if(random4<0.33)
                    {
                        enemyEgg3->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4 , Point( screenWidth/2, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.33 && random4<0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }
            }else if (random3 >=0.25 && random3 < 0.50)
            {
                enemyEgg2->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4 , Point( screenWidth/2, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
                    auto random4 = CCRANDOM_0_1();
                    if(random4<0.33)
                    {
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4 , Point( screenWidth/2 - 0.4 * laneWidth , -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.33 && random4<0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }
            }else if(random3 >=0.50 && random3 < 0.75)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
                    auto random4 = CCRANDOM_0_1();
                    if(random4<0.33)
                    {
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4 , Point( screenWidth/2 - 0.4 * laneWidth , -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.33 && random4<0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4, Point( screenWidth/2, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }
            }else if (random3 >= 0.75)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
                    auto random4 = CCRANDOM_0_1();
                    if(random4<0.33)
                    {
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4 , Point( screenWidth/2 - 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.33 && random4<0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4  , Point( screenWidth/2, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }
            }
        }
        if (Position == LanePosition::THREE)
        {
            enemyEgg->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
            layer->addChild( enemyEgg, ENEMY_LAYER);
            auto enemyEggAction = MoveTo::create( 4 , Point( screenWidth/2, -visibleSize.height/2 ) );
            enemyEgg->runAction( enemyEggAction );
            auto random3 = CCRANDOM_0_1();
            if(random3<0.25)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4 , Point( screenWidth/2 - 0.4 * laneWidth , -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
                    auto random4 = CCRANDOM_0_1();
                    if(random4<0.33)
                    {
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.2 * laneWidth, visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4 , Point( screenWidth/2 - 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.33 && random4<0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.4 * laneWidth, visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }
            }else if (random3 >=0.25 && random3 < 0.50)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4 , Point( screenWidth/2 - 0.2 * laneWidth , -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
                    auto random4 = CCRANDOM_0_1();
                    if(random4<0.33)
                    {
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4 , Point( screenWidth/2 - 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.33 && random4<0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }
            }else if(random3 >=0.50 && random3 < 0.75)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
                    auto random4 = CCRANDOM_0_1();
                    if(random4<0.33)
                    {
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4 , Point( screenWidth/2 - 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.33 && random4<0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4, Point( screenWidth/2 - 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }
                
            }else if (random3 >= 0.75)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
                    auto random4 = CCRANDOM_0_1();
                    if(random4<0.33)
                    {
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4 , Point( screenWidth/2 - 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.33 && random4<0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4  , Point( screenWidth/2 - 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }
            }
        }
        if (Position == LanePosition::FOUR)
        {
            enemyEgg->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
            layer->addChild( enemyEgg, ENEMY_LAYER);
            auto enemyEggAction = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
            enemyEgg->runAction( enemyEggAction );
            auto random3 = CCRANDOM_0_1();
            if(random3<0.25)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4 , Point( screenWidth/2 - 0.4 * laneWidth , -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
                    auto random4 = CCRANDOM_0_1();
                    if(random4<0.33)
                    {
                        enemyEgg3->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4 , Point( screenWidth/2, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.33 && random4<0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4, Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4  , Point( screenWidth/2 - 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }
            }else if (random3 >=0.25 && random3 < 0.50)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4 , Point( screenWidth/2 - 0.2 * laneWidth , -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
                    auto random4 = CCRANDOM_0_1();
                    if(random4<0.33)
                    {
                        enemyEgg3->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4 , Point( screenWidth/2, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.33 && random4<0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4, Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4  , Point( screenWidth/2 - 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }
            }else if(random3 >=0.50 && random3 < 0.75)
            {
                enemyEgg2->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4, Point( screenWidth/2, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
                    auto random4 = CCRANDOM_0_1();
                    if(random4<0.33)
                    {
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.4 *laneWidth, visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4 , Point( screenWidth/2 - 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.33 && random4<0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4, Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4  , Point( screenWidth/2 - 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }
            }else if (random3 >= 0.75)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
                    auto random4 = CCRANDOM_0_1();
                    if(random4<0.33)
                    {
                        enemyEgg3->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4 , Point( screenWidth/2, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.33 && random4<0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4, Point( screenWidth/2 - 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4  , Point( screenWidth/2 - 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }
            }
        }
        if (Position == LanePosition::FIVE)
        {
            enemyEgg->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
            layer->addChild( enemyEgg, ENEMY_LAYER);
            auto enemyEggAction = MoveTo::create( 4  , Point( screenWidth/2 + 0.4 * laneWidth, -visibleSize.height/2 ) );
            enemyEgg->runAction( enemyEggAction );
            auto random3 = CCRANDOM_0_1();
            if(random3<0.25)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4 , Point( screenWidth/2 - 0.4 * laneWidth , -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
                    auto random4 = CCRANDOM_0_1();
                    if(random4<0.33)
                    {
                        enemyEgg3->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4 , Point( screenWidth/2, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.33 && random4<0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4  , Point( screenWidth/2 - 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }
            }else if (random3 >=0.25 && random3 < 0.50)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4 , Point( screenWidth/2 - 0.2 * laneWidth , -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
                    auto random4 = CCRANDOM_0_1();
                    if(random4<0.33)
                    {
                        enemyEgg3->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4 , Point( screenWidth/2, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.33 && random4<0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4  , Point( screenWidth/2 - 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }
            }else if(random3 >=0.50 && random3 < 0.75)
            {
                enemyEgg2->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4, Point( screenWidth/2, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
                    auto random4 = CCRANDOM_0_1();
                    if(random4<0.33)
                    {
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.4 * laneWidth, visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4 , Point( screenWidth/2 - 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.33 && random4<0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4, Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4  , Point( screenWidth/2 - 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }
            }else if (random3 >= 0.75)
            {
                enemyEgg2->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                layer->addChild( enemyEgg2, ENEMY_LAYER);
                auto enemyEggAction2 = MoveTo::create( 4  , Point( screenWidth/2 + 0.2 * laneWidth, -visibleSize.height/2 ) );
                enemyEgg2->runAction( enemyEggAction2 );
                    auto random4 = CCRANDOM_0_1();
                    if(random4<0.33)
                    {
                        enemyEgg3->setPosition( Point( screenWidth/2, visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4 , Point( screenWidth/2, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.33 && random4<0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4, Point( screenWidth/2 - 0.4 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }else if(random4>=0.66){
                        enemyEgg3->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , visibleSize.height + visibleSize.height/2 ) );
                        layer->addChild( enemyEgg3, ENEMY_LAYER);
                        auto enemyEggAction3 = MoveTo::create( 4  , Point( screenWidth/2 - 0.2 * laneWidth, -visibleSize.height/2 ) );
                        enemyEgg3->runAction( enemyEggAction3 );
                    }
            }
        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//COINS SCRIPT

    /*Vector<SpriteFrame*> animFrames3(6);
    char str3[100] = {0};
    for(int i = 1; i <=6; i++)
    {
        sprintf(str3, "coin-%d",i);
        SpriteFrame* frame3 = cache->getSpriteFrameByName( str3 );
        animFrames3.pushBack(frame3);
    }
    auto animation3 = Animation::createWithSpriteFrames(animFrames3, 0.05f);
    coin->runAction(RepeatForever::create(Animate::create(animation3)));
    
    coinBody->setTag(spawntag);
    coin->setTag(spawntag);
    coin->setName("coin");
    coin->setPosition( Point(enemyEgg->getPositionX() , enemyEgg->getPositionY() + enemyEgg->getContentSize().height + coin->getContentSize().height) );
    layer->addChild( coin, ENEMY_LAYER -1);
    auto enemyEggAction = MoveTo::create( 5 , Point( enemyEgg->getPositionX() , -visibleSize.height/2 ) );
    coin->runAction( enemyEggAction );
     */
}



void Enemy::SpawnObstacle(cocos2d::Layer *layer , unsigned int spawnNum){
    visibleSize = Director::getInstance( )->getVisibleSize( );
    origin = Director::getInstance( )->getVisibleOrigin( );
    screenWidth = visibleSize.width;
    screenHeight = visibleSize.height;
    
    auto lane = Sprite::create("bg2.png");
    laneWidth=lane->getContentSize().width;
    laneHeight=lane->getContentSize().height;
    
    float obstacle_speed;
    if(visibleSize.height > LARGE_HEIGHT)
    {
        obstacle_speed = 0.0037;
    }else{
        obstacle_speed = 0.0075;
    }
    spawntag = spawnNum;
    if(spawntag%2){
        speedInc = spawntag * 0.1;
    }

    //MyBodyParser::getInstance()->parseJsonFile("obstacleBody.json");
    //auto enemyEggBody = MyBodyParser::getInstance()->bodyFormJson(enemyEgg, "obs1");
     Node *node = Node::create();
    enemyObstacle1Support = Sprite::create("stand.png");
    enemyObstacle1 = Sprite::create( "knife.png" );
    enemyObstacle1Shade = Sprite::create("shade.png");
    
    MyBodyParser::getInstance()->parseJsonFile("eggrush.json");
    auto enemyObstacle1Body = MyBodyParser::getInstance()->bodyFormJson(enemyObstacle1, "knife");
    enemyObstacle1Body->setDynamic( false );
    enemyObstacle1Body->setCollisionBitmask( MOVING_OBSTACLE_COLLISION_BITMASK );
    enemyObstacle1Body->setContactTestBitmask( true );
    enemyObstacle1->setPhysicsBody( enemyObstacle1Body );
    
    node->setPosition( Point( 0 , visibleSize.height + visibleSize.height/2 ) );
    
    auto random = CCRANDOM_0_1( );
    
    if ( random < 0.50 )
    {
        enemyObstacle1->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , 0) );
        auto action = MoveBy::create(2.0f, Point(laneWidth -enemyObstacle1->getContentSize().width,0));
        auto action_back = action->reverse();
        auto action2 = Sequence::create(action, action_back, nullptr);
        enemyObstacle1->runAction(action2);
    }
    else if ( random >=0.50)
    {
        enemyObstacle1->setPosition( Point(screenWidth/2 + 0.4 * laneWidth, 0) );
        auto action = MoveBy::create(2.0f, Point(-laneWidth+enemyObstacle1->getContentSize().width,0));
        auto action_back = action->reverse();
        auto action2 = Sequence::create(action, action_back, nullptr);
        enemyObstacle1->runAction(action2);
    }
    
    enemyObstacle1Support->setPosition( Point( screenWidth/2 , enemyObstacle1->getContentSize().height/3) );
    enemyObstacle1Shade->setPosition( screenWidth/2 , 0);
    
    node->addChild(enemyObstacle1, OBSTACLE_OBJECT_LAYER);
    node->addChild(enemyObstacle1Support, OBSTACLE_STAND_LAYER);
    node->addChild(enemyObstacle1Shade,OBSTACLE_SHADOW_LAYER);
    
    layer->addChild(node,HERO_LAYER);


    //auto action3 = Spawn::create(enemyObstacle1Action,action2, NULL);
    //NODE Y AXIS MOVEMENT
    
    auto enemyObstacle1Action = MoveTo::create( 4  , Point(0 , -visibleSize.height/2) );
    node->runAction(RepeatForever::create(enemyObstacle1Action));
    
}

void Enemy::SpawnCutter(cocos2d::Layer *layer , unsigned int spawnNum){
    visibleSize = Director::getInstance( )->getVisibleSize( );
    origin = Director::getInstance( )->getVisibleOrigin( );
    screenWidth = visibleSize.width;
    screenHeight = visibleSize.height;
    
    auto lane = Sprite::create("bg2.png");
    laneWidth=lane->getContentSize().width;
    laneHeight=lane->getContentSize().height;
    
    float obstacle_speed;
    if(visibleSize.height > LARGE_HEIGHT)
    {
        obstacle_speed = 0.0037;
    }else{
        obstacle_speed = 0.0075;
    }
    spawntag = spawnNum;
    if(spawntag%2){
        speedInc = spawntag * 0.1;
    }
    
    //MyBodyParser::getInstance()->parseJsonFile("obstacleBody.json");
    //auto enemyEggBody = MyBodyParser::getInstance()->bodyFormJson(enemyEgg, "obs1");
    Node *node = Node::create();
    enemyObstacle1Support = Sprite::create("stand.png");
    enemyObstacle1 = Sprite::create( "cutter.png" );
    enemyObstacle1Shade = Sprite::create("shade.png");
    
    SpriteFrameCache* cache = SpriteFrameCache::getInstance();
    cache->addSpriteFramesWithFile("enemy_obstacles.plist");
    Vector<SpriteFrame*> animFrames3(8);
    char str3[100] = {0};
    for(int i = 1; i <=8; i++)
    {
        sprintf(str3, "cutter-%d",i);
        SpriteFrame* frame3 = cache->getSpriteFrameByName( str3 );
        animFrames3.pushBack(frame3);
    }
    auto animation3 = Animation::createWithSpriteFrames(animFrames3, 0.02f);
    enemyObstacle1->runAction(RepeatForever::create(Animate::create(animation3)));
    
    MyBodyParser::getInstance()->parseJsonFile("eggrush.json");
    auto enemyObstacle1Body = MyBodyParser::getInstance()->bodyFormJson(enemyObstacle1, "cutter");
    enemyObstacle1Body->setDynamic( false );
    enemyObstacle1Body->setCollisionBitmask( MOVING_OBSTACLE_COLLISION_BITMASK );
    enemyObstacle1Body->setContactTestBitmask( true );
    enemyObstacle1->setPhysicsBody( enemyObstacle1Body );
    
    node->setPosition( Point( 0 , visibleSize.height + visibleSize.height/2 ) );
    
    auto random = CCRANDOM_0_1( );
    
    if ( random < 0.50 )
    {
        enemyObstacle1->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , 0) );
        auto action = MoveBy::create(2.0f, Point(laneWidth -enemyObstacle1->getContentSize().width,0));
        auto action_back = action->reverse();
        auto action2 = Sequence::create(action, action_back, nullptr);
        enemyObstacle1->runAction(action2);
    }
    else if ( random >=0.50)
    {
        enemyObstacle1->setPosition( Point(screenWidth/2 + 0.4 * laneWidth, 0) );
        auto action = MoveBy::create(2.0f, Point(-laneWidth+enemyObstacle1->getContentSize().width,0));
        auto action_back = action->reverse();
        auto action2 = Sequence::create(action, action_back, nullptr);
        enemyObstacle1->runAction(action2);
    }
    
    
    enemyObstacle1Support->setPosition( Point( screenWidth/2 , enemyObstacle1->getContentSize().height/3) );
    enemyObstacle1Shade->setPosition( screenWidth/2 , 0);
    
    node->addChild(enemyObstacle1, OBSTACLE_OBJECT_LAYER);
    node->addChild(enemyObstacle1Support, OBSTACLE_STAND_LAYER);
    node->addChild(enemyObstacle1Shade,OBSTACLE_SHADOW_LAYER);
    
    layer->addChild(node,HERO_LAYER);
    
    /*auto action = MoveBy::create(2.0f, Point(laneWidth -enemyObstacle1->getContentSize().width,0));
    auto action_back = action->reverse();
    auto action2 = Sequence::create(action, action_back, nullptr);
    
    enemyObstacle1->runAction(action2);*/
    //auto action3 = Spawn::create(enemyObstacle1Action,action2, NULL);
    //NODE Y AXIS MOVEMENT
    
    auto enemyObstacle1Action = MoveTo::create( 4  , Point(0 , -visibleSize.height/2 ) );
    node->runAction(RepeatForever::create(enemyObstacle1Action));
    
}

void Enemy::SpawnBlender(cocos2d::Layer *layer , unsigned int spawnNum){
    visibleSize = Director::getInstance( )->getVisibleSize( );
    origin = Director::getInstance( )->getVisibleOrigin( );
    screenWidth = visibleSize.width;
    screenHeight = visibleSize.height;
    
    auto lane = Sprite::create("bg2.png");
    laneWidth=lane->getContentSize().width;
    laneHeight=lane->getContentSize().height;
    
    float obstacle_speed;
    if(visibleSize.height > LARGE_HEIGHT)
    {
        obstacle_speed = 0.0037;
    }else{
        obstacle_speed = 0.0075;
    }
    spawntag = spawnNum;
    if(spawntag%2){
        speedInc = spawntag * 0.1;
    }
    
    //MyBodyParser::getInstance()->parseJsonFile("obstacleBody.json");
    //auto enemyEggBody = MyBodyParser::getInstance()->bodyFormJson(enemyEgg, "obs1");
    Node *node = Node::create();
    enemyObstacle1Support = Sprite::create("stand.png");
    enemyObstacle1 = Sprite::create( "blender.png" );
    enemyObstacle1Shade = Sprite::create("shade.png");
    
    MyBodyParser::getInstance()->parseJsonFile("eggrush.json");
    auto enemyObstacle1Body = MyBodyParser::getInstance()->bodyFormJson(enemyObstacle1, "blender");
    enemyObstacle1Body->setDynamic( false );
    enemyObstacle1Body->setCollisionBitmask( OBSTACLE_COLLISION_BITMASK );
    enemyObstacle1Body->setContactTestBitmask( true );
    enemyObstacle1->setPhysicsBody( enemyObstacle1Body );
    
    node->setPosition( Point( 0 , visibleSize.height + visibleSize.height/2 ) );
    
    auto random = CCRANDOM_0_1( );
    
    if ( random < 0.20 )
    {
        enemyObstacle1->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , 0) );
    }
    else if ( random >=0.20 && random < 0.40)
    {
        enemyObstacle1->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , 0) );
    }
    else if ( random >=0.40 && random < 0.60)
    {
        enemyObstacle1->setPosition( Point( screenWidth/2, 0) );
    }
    else if ( random >=0.60 && random < 0.80)
    {
        enemyObstacle1->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , 0) );
    }
    else if ( random >=0.80)
    {
        enemyObstacle1->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , 0) );
    }
    
    enemyObstacle1Support->setPosition( Point( screenWidth/2 , enemyObstacle1->getContentSize().height/3) );
    enemyObstacle1Shade->setPosition( screenWidth/2 , 0);
    
    node->addChild(enemyObstacle1, OBSTACLE_OBJECT_LAYER);
    node->addChild(enemyObstacle1Support, OBSTACLE_STAND_LAYER);
    node->addChild(enemyObstacle1Shade,OBSTACLE_SHADOW_LAYER);
    
    layer->addChild(node,HERO_LAYER);
    
    /*
    auto action = MoveBy::create(2.0f, Point(laneWidth -enemyObstacle1->getContentSize().width,0));
    auto action_back = action->reverse();
    auto action2 = Sequence::create(action, action_back, nullptr);
     */
    
    //enemyObstacle1->runAction(action2);
    //auto action3 = Spawn::create(enemyObstacle1Action,action2, NULL);
    //NODE Y AXIS MOVEMENT
    
    auto enemyObstacle1Action = MoveTo::create( 4  , Point(0 , -visibleSize.height/2) );
    node->runAction(RepeatForever::create(enemyObstacle1Action));
    
}

void Enemy::SpawnFlamer(cocos2d::Layer *layer , unsigned int spawnNum){
    visibleSize = Director::getInstance( )->getVisibleSize( );
    origin = Director::getInstance( )->getVisibleOrigin( );
    screenWidth = visibleSize.width;
    screenHeight = visibleSize.height;
    
    auto lane = Sprite::create("bg2.png");
    laneWidth=lane->getContentSize().width;
    laneHeight=lane->getContentSize().height;
    
    float obstacle_speed;
    if(visibleSize.height > LARGE_HEIGHT)
    {
        obstacle_speed = 0.0037;
    }else{
        obstacle_speed = 0.0075;
    }
    spawntag = spawnNum;
    if(spawntag%2){
        speedInc = spawntag * 0.1;
    }
    
    //MyBodyParser::getInstance()->parseJsonFile("obstacleBody.json");
    //auto enemyEggBody = MyBodyParser::getInstance()->bodyFormJson(enemyEgg, "obs1");
    Node *node = Node::create();
    enemyObstacle1Support = Sprite::create("stand.png");
    enemyObstacle1 = Sprite::create( "flamer.png" );
    enemyObstacle1Shade = Sprite::create("shade.png");
    
    SpriteFrameCache* cache = SpriteFrameCache::getInstance();
    cache->addSpriteFramesWithFile("enemy_obstacles.plist");
    Vector<SpriteFrame*> animFrames3(5);
    char str3[100] = {0};
    for(int i = 1; i <=5; i++)
    {
        sprintf(str3, "flamer-%d",i);
        SpriteFrame* frame3 = cache->getSpriteFrameByName( str3 );
        animFrames3.pushBack(frame3);
    }
    auto animation3 = Animation::createWithSpriteFrames(animFrames3, 0.05f);
    enemyObstacle1->runAction(RepeatForever::create(Animate::create(animation3)));
    
    MyBodyParser::getInstance()->parseJsonFile("eggrush.json");
    auto enemyObstacle1Body = MyBodyParser::getInstance()->bodyFormJson(enemyObstacle1, "flamer");
    enemyObstacle1Body->setDynamic( false );
    enemyObstacle1Body->setCollisionBitmask( OBSTACLE_COLLISION_BITMASK );
    enemyObstacle1Body->setContactTestBitmask( true );
    enemyObstacle1->setPhysicsBody( enemyObstacle1Body );
    
    node->setPosition( Point( 0 , visibleSize.height + visibleSize.height/2 ) );
    
    auto random = CCRANDOM_0_1( );
    
    if ( random < 0.20 )
    {
        enemyObstacle1->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , 0) );
    }
    else if ( random >=0.20 && random < 0.40)
    {
        enemyObstacle1->setPosition( Point( screenWidth/2 - 0.2 * laneWidth , 0) );
    }
    else if ( random >=0.40 && random < 0.60)
    {
        enemyObstacle1->setPosition( Point( screenWidth/2, 0) );
    }
    else if ( random >=0.60 && random < 0.80)
    {
        enemyObstacle1->setPosition( Point( screenWidth/2 + 0.2 * laneWidth , 0) );
    }
    else if ( random >=0.80)
    {
        enemyObstacle1->setPosition( Point( screenWidth/2 + 0.4 * laneWidth , 0) );
    }
    //enemyObstacle1->setPosition( Point( screenWidth/2 - 0.4 * laneWidth , 0) );
    enemyObstacle1Support->setPosition( Point( screenWidth/2 , enemyObstacle1->getContentSize().height/3) );
    enemyObstacle1Shade->setPosition( screenWidth/2 , 0);
    
    node->addChild(enemyObstacle1, OBSTACLE_OBJECT_LAYER);
    node->addChild(enemyObstacle1Support, OBSTACLE_STAND_LAYER);
    node->addChild(enemyObstacle1Shade,OBSTACLE_SHADOW_LAYER);
    
    layer->addChild(node,HERO_LAYER);
    
    /*auto action = MoveBy::create(2.0f, Point(laneWidth -enemyObstacle1->getContentSize().width,0));
    auto action_back = action->reverse();
    auto action2 = Sequence::create(action, action_back, nullptr);
    enemyObstacle1->runAction(action2);*/
    //auto action3 = Spawn::create(enemyObstacle1Action,action2, NULL);
    //NODE Y AXIS MOVEMENT
    
    auto enemyObstacle1Action = MoveTo::create( 4  , Point(0 , -visibleSize.height/2 ) );
    node->runAction(RepeatForever::create(enemyObstacle1Action));
    
}